package errormessage;

public class ErrorMessage {
	public static final String INCORRECT_USER = "正しいユーザ名、パスワードを入力してください";
	public static final String NO_MATCH_REKEYPASS="再入力されたパスワードの内容に間違いがあります";
	public static final String NO_CONSENT_AGREEMENT="利用規約に同意いただけない方は登録できません";
	public static final String NO_MATCH_NAME="入力されたユーザ名の内容に間違いがあります";
	public static final String NO_MATCH_PASS="入力されたパスワードの内容に間違いがあります";
	public static final String MATCH_PASS_OR_NAME="入力されたユーザ名またはパスワードはすでに存在しています";
	public static final String DELETEUSER = "違反行為に反したため削除されました";
}
