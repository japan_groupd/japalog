package jp.co.phopan.form;

import org.apache.struts.validator.ValidatorForm;

public class ReportForm extends ValidatorForm {
	private int img_id;
    private String report;
    private int copyright;
    private int portrait;
    private int infringement;
    private int sexual;
    private int userid;
    private int imgid;

	public int getImg_id() {
		return img_id;
	}
	public void setImg_id(int img_id) {
		this.img_id = img_id;
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public int getCopyright() {
		return copyright;
	}
	public void setCopyright(int copyright) {
		this.copyright = copyright;
	}
	public int getPortrait() {
		return portrait;
	}
	public void setPortrait(int portrait) {
		this.portrait = portrait;
	}
	public int getInfringement() {
		return infringement;
	}
	public void setInfringement(int infringement) {
		this.infringement = infringement;
	}
	public int getSexual() {
		return sexual;
	}
	public void setSexual(int sexual) {
		this.sexual = sexual;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public int getImgid() {
		return imgid;
	}
	public void setImgid(int imgid) {
		this.imgid = imgid;
	}
}
