package jp.co.phopan.form;

import org.apache.struts.upload.FormFile;
import org.apache.struts.validator.ValidatorForm;

public class PhotoForm extends ValidatorForm {

	private int img_id;
	private String img_name;
	private String title;
	private int category_id;
	private String category_name;
	private String details;
	private FormFile fileup;
	private int good;
 	private int total_good;
	private int week_good;
	private int day_good;
	private int userid;

	public int getImg_id() {
		return img_id;
	}
	public void setImg_id(int img_id) {
		this.img_id = img_id;
	}
	public FormFile getFileup() {
		return fileup;
	}
	public void setFileup(FormFile fileup) {
		this.fileup = fileup;
	}
	public String getImg_name() {
		return img_name;
	}
	public void setImg_name(String img_name) {
		this.img_name = img_name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public int getTotal_good() {
		return total_good;
	}
	public void setTotal_good(int total_good) {
		this.total_good = total_good;
	}
	public int getWeek_good() {
		return week_good;
	}
	public void setWeek_good(int week_good) {
		this.week_good = week_good;
	}
	public int getDay_good() {
		return day_good;
	}
	public void setDay_good(int day_good) {
		this.day_good = day_good;
	}
	public int getGood() {
		return good;
	}
	public void setGood(int good) {
		this.good = good;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
}
