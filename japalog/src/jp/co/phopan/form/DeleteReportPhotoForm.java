package jp.co.phopan.form;

import java.util.ArrayList;

import jp.co.phopan.db.ImageTable;

import org.apache.struts.action.ActionForm;

public class DeleteReportPhotoForm extends ActionForm {
	private int img_id;
	private String img_name;
	private int report;
	private int copyright;
	private int portrait;
	private int infringement;
	private int sexual;
	private boolean delete;
	private int user_id;
	private String user_name;
	private ArrayList<ImageTable> list;


	public ArrayList<ImageTable> getList() {
		return list;
	}
	public void setList(ArrayList<ImageTable> list) {
		this.list = list;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public int getImg_id() {
		return img_id;
	}
	public void setImg_id(int img_id) {
		this.img_id = img_id;
	}
	public String getImg_name() {
		return img_name;
	}
	public void setImg_name(String img_name) {
		this.img_name = img_name;
	}
	public int getReport() {
		return report;
	}
	public void setReport(int report) {
		this.report = report;
	}
	public int getCopyright() {
		return copyright;
	}
	public void setCopyright(int copyright) {
		this.copyright = copyright;
	}
	public int getPortrait() {
		return portrait;
	}
	public void setPortrait(int portrait) {
		this.portrait = portrait;
	}
	public int getInfringement() {
		return infringement;
	}
	public void setInfringement(int infringement) {
		this.infringement = infringement;
	}
	public int getSexual() {
		return sexual;
	}
	public void setSexual(int sexual) {
		this.sexual = sexual;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
}
