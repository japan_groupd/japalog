package jp.co.phopan.form;

import java.util.ArrayList;

import jp.co.phopan.db.ImageTable;

import org.apache.struts.validator.ValidatorForm;

public class LoginForm extends ValidatorForm {
	private String user_name;
	private String pass;
	private String rekeypass;
	private int user_id;
	private boolean user_delete;
	private String consent;
	private ArrayList<ImageTable> list;

	public ArrayList<ImageTable> getList() {
		return list;
	}
	public void setList(ArrayList<ImageTable> list) {
		this.list = list;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public boolean isUser_delete() {
		return user_delete;
	}
	public void setUser_delete(boolean user_delete) {
		this.user_delete = user_delete;
	}
	public String getRekeypass() {
		return rekeypass;
	}
	public void setRekeypass(String rekeypass) {
		this.rekeypass = rekeypass;
	}
	public String getConsent() {
		return consent;
	}
	public void setConsent(String consent) {
		this.consent = consent;
	}


}
