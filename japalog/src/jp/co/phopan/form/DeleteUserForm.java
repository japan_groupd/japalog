package jp.co.phopan.form;

import org.apache.struts.action.ActionForm;

public class DeleteUserForm extends ActionForm  {

	private int User_id;
	private  String User_name;
	private String pass;

	public int getUser_id() {
		return User_id;
	}
	public void setUser_id(int user_id) {
		User_id = user_id;
	}
	public String getUser_name() {
		return User_name;
	}
	public void setUser_name(String user_name) {
		User_name = user_name;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}


}
