package jp.co.phopan.form;

import org.apache.struts.action.ActionForm;

public class ActionForNameForm  extends ActionForm {
	  private boolean disabledNextButton = false;
	  public boolean isDisabledNextButtont() {
	    return disabledNextButton;
	  }
	  public void setDisabledNextButton(boolean disabledNextButton) {
	    this.disabledNextButton = disabledNextButton;
	  }
	}