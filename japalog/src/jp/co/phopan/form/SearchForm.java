package jp.co.phopan.form;

import org.apache.struts.validator.ValidatorForm;

public class SearchForm extends ValidatorForm {
    private String category_id;
    private String keyword;
    private String sort;


	public String getCategory_id() {
		return category_id;
	}
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}

}
