package jp.co.phopan.form;

import org.apache.struts.validator.ValidatorForm;

public class ProfileForm extends ValidatorForm{

	private String olduser_name;
	private String newuser_name;
	private String oldpass;
	private String newpass;
	private String newrekeypass;

	public String getOlduser_name() {
		return olduser_name;
	}
	public void setOlduser_name(String olduser_name) {
		this.olduser_name = olduser_name;
	}
	public String getNewuser_name() {
		return newuser_name;
	}
	public void setNewuser_name(String newuser_name) {
		this.newuser_name = newuser_name;
	}
	public String getOldpass() {
		return oldpass;
	}
	public void setOldpass(String oldpass) {
		this.oldpass = oldpass;
	}
	public String getNewpass() {
		return newpass;
	}
	public void setNewpass(String newpass) {
		this.newpass = newpass;
	}
	public String getNewrekeypass() {
		return newrekeypass;
	}
	public void setNewrekeypass(String newrekeypass) {
		this.newrekeypass = newrekeypass;
	}

}
