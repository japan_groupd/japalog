package jp.co.phopan.db;

public class UserTable {
	private int user_id;
	private String user_name;
	private String pass;
	private boolean user_delete;
	private boolean checked;
	private String[] check;


	public String[] getCheck() {
		return check;
	}
	public void setCheck(String[] check) {
		this.check = check;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public boolean isUser_delete() {
		return user_delete;
	}
	public void setUser_delete(boolean user_delete) {
		this.user_delete = user_delete;
	}
}
