package jp.co.phopan.db;

public class JoinTable {

	/* ImageTable */
	private int img_id;
	private int user_id;
	private String img_name;
	private int category_id;
	private String title;
	private String uploadtime;
	private String details;
	private int total_good;
	private int week_good;
	private int day_good;
	private int access_count;
	private int report;
	private int copyright;
	private int portrait;
	private int infringement;
	private int sexual;
	private boolean img_delete;

	/* UserTable */
	private String user_name;
	private String pass;
	private boolean user_delete;

	/* CategoryTable */
	private String category_name;

	public int getImg_id() {
		return img_id;
	}

	public void setImg_id(int img_id) {
		this.img_id = img_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getImg_name() {
		return img_name;
	}

	public void setImg_name(String img_name) {
		this.img_name = img_name;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUploadtime() {
		return uploadtime;
	}

	public void setUploadtime(String uploadtime) {
		this.uploadtime = uploadtime;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public int getTotal_good() {
		return total_good;
	}

	public void setTotal_good(int total_good) {
		this.total_good = total_good;
	}

	public int getWeek_good() {
		return week_good;
	}

	public void setWeek_good(int week_good) {
		this.week_good = week_good;
	}

	public int getDay_good() {
		return day_good;
	}

	public void setDay_good(int day_good) {
		this.day_good = day_good;
	}

	public int getAccess_count() {
		return access_count;
	}

	public void setAccess_count(int access_count) {
		this.access_count = access_count;
	}

	public int getReport() {
		return report;
	}

	public void setReport(int report) {
		this.report = report;
	}

	public int getCopyright() {
		return copyright;
	}

	public void setCopyright(int copyright) {
		this.copyright = copyright;
	}

	public int getPortrait() {
		return portrait;
	}

	public void setPortrait(int portrait) {
		this.portrait = portrait;
	}

	public int getInfringement() {
		return infringement;
	}

	public void setInfringement(int infringement) {
		this.infringement = infringement;
	}

	public int getSexual() {
		return sexual;
	}

	public void setSexual(int sexual) {
		this.sexual = sexual;
	}

	public boolean isImg_delete() {
		return img_delete;
	}

	public void setImg_delete(boolean img_delete) {
		this.img_delete = img_delete;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public boolean isUser_delete() {
		return user_delete;
	}

	public void setUser_delete(boolean user_delete) {
		this.user_delete = user_delete;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
}
