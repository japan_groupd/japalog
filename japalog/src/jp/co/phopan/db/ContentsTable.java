package jp.co.phopan.db;

public class ContentsTable {
	private int img_id;
	private int user_id;
	private int category_id;
	private String title;
	private String uploadtitle;
	private String details;
	private int total_good;
	private int day_good;
	private int access_count;

	public int getImg_id() {
		return img_id;
	}
	public void setImg_id(int img_id) {
		this.img_id = img_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUploadtitle() {
		return uploadtitle;
	}
	public void setUploadtitle(String uploadtitle) {
		this.uploadtitle = uploadtitle;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public int getTotal_good() {
		return total_good;
	}
	public void setTotal_good(int total_good) {
		this.total_good = total_good;
	}
	public int getDay_good() {
		return day_good;
	}
	public void setDay_good(int day_good) {
		this.day_good = day_good;
	}
	public int getAccess_count() {
		return access_count;
	}
	public void setAccess_count(int access_count) {
		this.access_count = access_count;
	}
}
