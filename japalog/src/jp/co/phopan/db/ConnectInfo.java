package jp.co.phopan.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class ConnectInfo {
	private Connection my_con = null;
	private Statement my_stmt = null;
	private ResultSet my_Result = null;

	public Connection getMy_con() {
		return my_con;
	}
	public void setMy_con(Connection my_con) {
		this.my_con = my_con;
	}
	public Statement getMy_stmt() {
		return my_stmt;
	}
	public void setMy_stmt(Statement my_stmt) {
		this.my_stmt = my_stmt;
	}
	public ResultSet getMy_Result() {
		return my_Result;
	}
	public void setMy_Result(ResultSet my_Result) {
		this.my_Result = my_Result;
	}
}
