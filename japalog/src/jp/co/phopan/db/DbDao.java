package jp.co.phopan.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class DbDao {
	public static final String SELECT_ALL_SQL = "SELECT * FROM ";


	public static ResultSet selectNew(Statement inputStmt)throws SQLException{
		String sql = "SELECT * FROM imagetable LEFT JOIN usertable ON usertable.user_id=imagetable.user_id LEFT JOIN categorytable ON categorytable.category_id=imagetable.category_id";

		sql += " WHERE imagetable.img_delete=true";
		sql += " ORDER BY imagetable.img_id DESC LIMIT 5";

		return inputStmt.executeQuery(sql);
	}


	public static ResultSet selectSearch(Statement inputStmt, String title, String category_id, String orderby)throws SQLException{
		String sql = "SELECT * FROM imagetable LEFT JOIN usertable ON usertable.user_id=imagetable.user_id LEFT JOIN categorytable ON categorytable.category_id=imagetable.category_id";

		sql += " WHERE imagetable.img_delete=true";

		if( Integer.parseInt(category_id) != 0 ){
			sql += " AND imagetable.category_id=" + category_id;
		}

		//検索ワードがあったら
		if( title != null && title.length() != 0 && !title.equals("") ) {
			sql += " AND imagetable.title LIKE '%" + title + "%'";
		}

		sql += orderby;

		return inputStmt.executeQuery(sql);
	}


	public static String selectCategory(int category_id){

		try {
			String name = "";
		String sql="SELECT * FROM categorytable where category_id=?";

		//データベース接続
		Connection con =DbUtil.connect();

		PreparedStatement stmt;

		stmt = con.prepareStatement(sql);

		stmt.setInt(1, category_id);

		ResultSet rs = stmt.executeQuery();

		while(rs.next()) {
			name=rs.getString("category_name");
		}

		//接続を切断
		stmt.close();
		DbUtil.disconnect(con);

		return name;

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();

			return null;
		}

	}

	public static ResultSet selectUser(Connection con)throws SQLException{

		String sql="SELECT*FROM UserTable;";

		PreparedStatement stmt = con.prepareStatement(sql);

		return stmt.executeQuery();

	}

	public static ResultSet select_serchUsername(Connection con, String name)throws SQLException{

		String sql="SELECT*FROM UserTable WHERE user_name= ?";

		PreparedStatement stmt = con.prepareStatement(sql);
		stmt.setString(1,name);
		return stmt.executeQuery();

	}

	public static ResultSet select_serchUserid(Connection con, int id)throws SQLException{

		String sql="SELECT*FROM UserTable WHERE user_id= ?";

		PreparedStatement stmt = con.prepareStatement(sql);
		stmt.setInt(1,id);
		return stmt.executeQuery();

	}


	public static void InsertImage(int user_id ,String img_name, int category_id,String title,String details){

		try {

			//データベース接続
			Connection con =DbUtil.connect();

			String sql ="INSERT INTO imagetable ( user_id ,img_name,category_id,title,uploadtime,details) VALUES (?,?,?,?,?,?);";
			PreparedStatement stmt;

			stmt = con.prepareStatement(sql);

			java.util.Date d = new java.util.Date();
			java.sql.Date date = new java.sql.Date(d.getTime());

			//Date date = format.parse("str_date");
			stmt.setInt(1, user_id);
			stmt.setString(2, img_name);
			stmt.setInt(3, category_id);
			stmt.setString(4, title);
			stmt.setDate(5,date);
			stmt.setString(6, details);

			stmt.executeUpdate();

			//接続を切断
			stmt.close();
			DbUtil.disconnect(con);


		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void InsertUser(Connection con,String user_name, String pass)throws SQLException{

			String sql ="INSERT INTO usertable (user_name ,pass) VALUES (?,?);";
			PreparedStatement stmt;

			stmt = con.prepareStatement(sql);

			stmt.setString(1,user_name);
			stmt.setString(2,pass);
			stmt.executeUpdate();

		}

	public static void UpdateImage(int img_id, int category_id,String title,String details) throws SQLException{

		try {

			//データベース接続
			Connection con =DbUtil.connect();

			String sql = "UPDATE IMAGETABLE SET category_id=?,title=?,details=? WHERE img_id=?";;
			PreparedStatement stmt = con.prepareStatement(sql);

			stmt.setInt(1, category_id);
			stmt.setString(2, title);
			stmt.setString(3, details);
			stmt.setInt(4,img_id);

			stmt.executeUpdate();

			//接続を切断
			stmt.close();
			DbUtil.disconnect(con);

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void UpdateUser(Connection con,String old_name,String new_name,String pass) throws SQLException{

		String sql = "UPDATE usertable SET user_name=?,pass=? WHERE user_name=?";;
		PreparedStatement stmt = con.prepareStatement(sql);
		stmt.setString(1, new_name);
		stmt.setString(2, pass);
		stmt.setString(3, old_name);
		stmt.executeUpdate();

	}

	public static void DeleteImage(int img_id){

		try {

			//データベース接続
			Connection con =DbUtil.connect();

			String sql ="UPDATE imagetable set img_delete=false where img_id=?";
			PreparedStatement stmt;

			stmt = con.prepareStatement(sql);

			stmt.setInt(1, img_id);

			stmt.executeUpdate();

			//接続を切断
			stmt.close();
			DbUtil.disconnect(con);

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void updeteUser(String new_name, String new_pass) throws SQLException{

		try {

			//データベース接続
			Connection con =DbUtil.connect();

			String sql = "UPDATE USERTABLE SET user_name=?,pass=?, WHERE user_id=?";;
			PreparedStatement stmt = con.prepareStatement(sql);

			stmt.setString(1, new_name);
			stmt.setString(2, new_pass);

			stmt.executeUpdate();

			//接続を切断
			stmt.close();
			DbUtil.disconnect(con);

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}


	public static ResultSet select(Statement inputStmt,String inputTable)throws SQLException{
		String sql  = SELECT_ALL_SQL + inputTable;
		//SQL実行（データ取得）
		return inputStmt.executeQuery(sql);
	}


	public static ArrayList<Integer> selectCategoryCount(){

		try {
		ArrayList<Integer> category_NameCount1 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount2 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount3 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount4 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount5 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount6 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount7 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount8 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount9 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount10 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount11 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount12 = new ArrayList<Integer>();
		ArrayList<Integer> category_NameCount = new ArrayList<Integer>();

		int category1 = 0;
		int category2 = 0;
		int category3 = 0;
		int category4 = 0;
		int category5 = 0;
		int category6 = 0;
		int category7 = 0;
		int category8 = 0;
		int category9 = 0;
		int category10 = 0;
		int category11 = 0;
		int category12 = 0;

		//データベース接続
		Connection con =DbUtil.connect();

		PreparedStatement stmt;

		//12カテゴリのループ
		String sql="SELECT * FROM imagetable where category_id=1 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			category_NameCount1.add(rs.getInt("category_id"));
		}
		category1 =category_NameCount1.size();

		sql="SELECT * FROM imagetable where category_id=2 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs2 = stmt.executeQuery();
		while(rs2.next()) {
			category_NameCount2.add(rs2.getInt("category_id"));
		}
		category2 =category_NameCount2.size();

		sql="SELECT * FROM imagetable where category_id=3 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs3 = stmt.executeQuery();
		while(rs3.next()) {
			category_NameCount3.add(rs3.getInt("category_id"));
		}
		category3 =category_NameCount3.size();

		sql="SELECT * FROM imagetable where category_id=4 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs4 = stmt.executeQuery();
		while(rs4.next()) {
			category_NameCount4.add(rs4.getInt("category_id"));
		}
		category4 =category_NameCount4.size();

		sql="SELECT * FROM imagetable where category_id=5 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs5 = stmt.executeQuery();
		while(rs5.next()) {
			category_NameCount5.add(rs5.getInt("category_id"));
		}
		category5 =category_NameCount5.size();

		sql="SELECT * FROM imagetable where category_id=6 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs6 = stmt.executeQuery();
		while(rs6.next()) {
			category_NameCount6.add(rs6.getInt("category_id"));
		}
		category6 =category_NameCount6.size();

		sql="SELECT * FROM imagetable where category_id=7 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs7 = stmt.executeQuery();
		while(rs7.next()) {
			category_NameCount7.add(rs7.getInt("category_id"));
		}
		category7 =category_NameCount7.size();

		sql="SELECT * FROM imagetable where category_id=8 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs8 = stmt.executeQuery();
		while(rs8.next()) {
			category_NameCount8.add(rs8.getInt("category_id"));
		}
		category9 =category_NameCount8.size();

		sql="SELECT * FROM imagetable where category_id=9 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs9 = stmt.executeQuery();
		while(rs9.next()) {
			category_NameCount9.add(rs9.getInt("category_id"));
		}
		category9 =category_NameCount9.size();

		sql="SELECT * FROM imagetable where category_id=10 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs10 = stmt.executeQuery();
		while(rs10.next()) {
			category_NameCount10.add(rs10.getInt("category_id"));
		}
		category10 =category_NameCount10.size();

		sql="SELECT * FROM imagetable where category_id=11 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs11 = stmt.executeQuery();
		while(rs11.next()) {
			category_NameCount11.add(rs11.getInt("category_id"));
		}
		category11 =category_NameCount11.size();

		sql="SELECT * FROM imagetable where category_id=12 AND img_delete=true";
		stmt = con.prepareStatement(sql);
		ResultSet rs12 = stmt.executeQuery();
		while(rs12.next()) {
			category_NameCount12.add(rs12.getInt("category_id"));
		}
		category12 =category_NameCount12.size();

		//全カテゴリの件数を入力
		category_NameCount.add(category1);
		category_NameCount.add(category2);
		category_NameCount.add(category3);
		category_NameCount.add(category4);
		category_NameCount.add(category5);
		category_NameCount.add(category6);
		category_NameCount.add(category7);
		category_NameCount.add(category8);
		category_NameCount.add(category9);
		category_NameCount.add(category10);
		category_NameCount.add(category11);
		category_NameCount.add(category12);

		//接続を切断
		stmt.close();
		DbUtil.disconnect(con);

		return category_NameCount;

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();

			return null;
		}

	}
}
