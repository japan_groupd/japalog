package jp.co.phopan.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.phopan.db.DbDao;
import jp.co.phopan.form.PhotoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;


public class InsertPhotoAction extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		PhotoForm photoForm = (PhotoForm)form;
		String imgname = photoForm.getImg_name();
		String title = photoForm.getTitle();
		int category_id =photoForm.getCategory_id();
		String details =photoForm.getDetails();
		FormFile fileup = photoForm.getFileup();

		int fileSize = fileup.getFileSize();
		String category_name = DbDao.selectCategory(category_id);

		String move="bad";

		if (fileSize > 0) {

				int len = fileup.getFileName().length();
				byte[] bytes = fileup.getFileName().getBytes();
			//拡張子を確認
			if(!(fileup.getFileName().endsWith("jpg")||fileup.getFileName().endsWith("jpeg")
					||fileup.getFileName().endsWith("png")||fileup.getFileName().endsWith("gif"))){

				//エラーメッセージ設定
				request.setAttribute("imgerror", "・ .jpg .jpeg .png .gif の拡張子の画像を選択して下さい\n");
				move="bad";

			}else if( len != bytes.length ){
				//エラーメッセージ設定
				request.setAttribute("imgerror", "・画像の名前は半角英数字にしてください\n");
				move="bad";
			}else{

				//表示させるフォルダ内に保管
				InputStream is = fileup.getInputStream();

				BufferedInputStream inBuffer = new BufferedInputStream(is);

				FileOutputStream fos = new FileOutputStream("C:/git/git/japalog/japalog/WebContent/contents/"+fileup.getFileName());

				BufferedOutputStream outBuffer = new BufferedOutputStream(fos);

				int contents = 0;

				while ((contents = inBuffer.read()) != -1) {
					outBuffer.write(contents);
				}

				outBuffer.flush();
				inBuffer.close();
				outBuffer.close();


				//eclipseの読み込み用フォルダに保管
				InputStream is2 = fileup.getInputStream();

				BufferedInputStream inBuffer2 = new BufferedInputStream(is2);

				FileOutputStream fos2 = new FileOutputStream("C:/workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/japalog/contents/"+fileup.getFileName());

				BufferedOutputStream outBuffer2 = new BufferedOutputStream(fos2);

				int contents2 = 0;

				while ((contents2 = inBuffer2.read()) != -1) {
					outBuffer2.write(contents2);
				}

				outBuffer2.flush();
				inBuffer2.close();
				outBuffer2.close();


				request.setAttribute("title",title);
				request.setAttribute("category_id",category_id);
				request.setAttribute("category_name",category_name);

				photoForm.setCategory_name(category_name);
				photoForm.setDetails(details);
				photoForm.setImg_name(fileup.getFileName());

				request.setAttribute("fileName","contents/"+fileup.getFileName());
				move="success";

			}

			fileup.destroy();
		}else{
			//エラーメッセージ設定
			request.setAttribute("imgerror", "・投稿写真 必須項目が未選択です\n");
			move="bad";
		}
		if(title.equals("")){
			//エラーメッセージ設定
			request.setAttribute("titleerror", "・タイトル 必須項目が未入力です\n");
			move="bad";
		}else if(title.length()>13){
			//エラーメッセージ設定
			request.setAttribute("titleerror", "・タイトルは13 文字以下で入力して下さい\n");
			move="bad";
		}
		if(details.equals("")){
			//エラーメッセージ設定
			request.setAttribute("deterror", "・詳細 必須項目が未入力です\n");
			move="bad";
		}else if(title.length()>200){
			//エラーメッセージ設定
			request.setAttribute("deterror", "・詳細は200 文字以下で入力して下さい\n");
			move="bad";
		}


		return mapping.findForward(move);
	}
}
