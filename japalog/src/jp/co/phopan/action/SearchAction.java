package jp.co.phopan.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.phopan.db.ConnectInfo;
import jp.co.phopan.db.DbDao;
import jp.co.phopan.db.DbUtil;
import jp.co.phopan.db.JoinTable;
import jp.co.phopan.form.SearchForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class SearchAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) throws Exception {
		SearchForm inform = (SearchForm) form;

		//▼値を受け取って初期化
        String category_id = inform.getCategory_id();
        String sort = inform.getSort();
		String keyword = inform.getKeyword();
        String sorttext = "";										//〜〜順のテキスト
		String orderby = " ORDER BY imagetable.";								//ORDER BY設定
		int i = 0;
		ConnectInfo info = new ConnectInfo();						//DB接続情報

		List<JoinTable> joinAry = new ArrayList<JoinTable>();		//全テーブル要素を持ったリスト

		//sortの判断
		switch(sort){
		case "0":			//投稿日時が新しい順
			orderby +="img_id DESC";
			sorttext="投稿日時が新しい順";
			break;
		case "1":			//投稿日時が古い順
			orderby +="img_id ASC";
			sorttext="投稿日時が古い順";
			break;
		case "2":			//いいなぁが多い順
			orderby +="total_good DESC";
			sorttext="いいなぁが多い順";
			break;
		case "3":			//いいなぁが少ない順
			orderby +="total_good ASC";
			sorttext="いいなぁが少ない順";
			break;
		}

		try {
			//=============================================================================================================
			info.setMy_con(DbUtil.connect());						//Connection接続
			info.setMy_stmt(DbUtil.stmt(info.getMy_con()));			//Statement接続


			//JOIN文を使用して全件検索
			info.setMy_Result(DbDao.selectSearch(info.getMy_stmt(),keyword,category_id,orderby));
			while( (info.getMy_Result()).next() ){
				JoinTable objAl=new JoinTable();
				objAl.setImg_name(info.getMy_Result().getString("img_name"));
				objAl.setImg_delete(info.getMy_Result().getBoolean("img_delete"));
				objAl.setUser_name(info.getMy_Result().getString("user_name"));
				objAl.setUser_delete(info.getMy_Result().getBoolean("user_delete"));
				objAl.setCategory_name(info.getMy_Result().getString("category_name"));
				objAl.setTitle(info.getMy_Result().getString("title"));
				objAl.setUploadtime(info.getMy_Result().getString("uploadtime"));
				objAl.setTotal_good(info.getMy_Result().getInt("total_good"));
				objAl.setAccess_count(info.getMy_Result().getInt("access_count"));
				objAl.setImg_id(info.getMy_Result().getInt("img_id"));
				joinAry.add(objAl);
				i++;
			}
			//=============================================================================================================
			DbUtil.disstmt(info.getMy_stmt());					//Statement切断
			DbUtil.disconnect(info.getMy_con());					//Connection切断
		} catch(Exception e) {
			e.printStackTrace();
		}


        request.setAttribute("c",category_id);
        if( category_id.equals("0") ){
        	request.setAttribute("catename", "全カテゴリ");
        } else {
        	request.setAttribute("catename", DbDao.selectCategory(Integer.parseInt(category_id)));
        }
        request.setAttribute("count", ""+i);
        request.setAttribute("k", keyword);
        request.setAttribute("s", sort);
        request.setAttribute("stext", sorttext);
        request.setAttribute("joinArray", joinAry);

        return mapping.findForward("success");
    }
}