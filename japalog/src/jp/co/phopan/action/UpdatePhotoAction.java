package jp.co.phopan.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.phopan.db.DbDao;
import jp.co.phopan.db.DbUtil;
import jp.co.phopan.form.PhotoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class UpdatePhotoAction  extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {

		PhotoForm photoForm = (PhotoForm)form;
		HttpSession session = request.getSession(false);

		int imgid =Integer.parseInt((String)request.getParameter("img_id"));
		String imgname = (String)request.getParameter("img_name");
		int category_id =Integer.parseInt((String)request.getParameter("category_id"));
		String category_name = DbDao.selectCategory(category_id);
		String title = (String)request.getParameter("title");
		String details = (String)request.getParameter("introduction");

		String move="success";

			//次の画面へ値を保存
			request.setAttribute("title",title);
			request.setAttribute("category_id",category_id);
			request.setAttribute("category_name",category_name);

			photoForm.setImg_name(imgname);
			photoForm.setCategory_name(category_name);
			photoForm.setTitle(title);
			photoForm.setDetails(details);
			session.setAttribute("img_id", imgid);

			request.setAttribute("fileName","contents/"+imgname);


			//更新されたかの確認用の値を取得
			String sql="SELECT * FROM imagetable where img_id=?";

			//データベース接続
			Connection con =DbUtil.connect();

			PreparedStatement stmt = con.prepareStatement(sql);

			stmt.setInt(1, imgid);

			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				if(rs.getString("title").equals(title)&&rs.getString("details").equals(details)&&rs.getInt("category_id")==category_id){
					request.setAttribute("error", "・タイトル 詳細 カテゴリのいずれかを変更してください\n");
					move="bad";
				}
			}

			if(title.equals("")){
				request.setAttribute("titleerror", "・タイトル 必須項目が未入力です\n");
				move="bad";
			}else if(title.length()>=13){
				request.setAttribute("titleerror", "・タイトルは13 文字以下で入力して下さい\n");
				move="bad";
			}
			if(details.equals("")){
				request.setAttribute("deterror", "・詳細 必須項目が未入力です\n");
				move="bad";
			}else if(title.length()>=200){
				request.setAttribute("deterror", "・詳細は200 文字以下で入力して下さい\n");
				move="bad";
			}

		return mapping.findForward(move);

	}

}
