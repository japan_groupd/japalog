package jp.co.phopan.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.phopan.db.DbDao;
import jp.co.phopan.db.DbUtil;
import jp.co.phopan.db.ImageTable;
import jp.co.phopan.form.DeleteReportPhotoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DeleteReportPhotoConfirmAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		DeleteReportPhotoForm deletereportphotoForm = (DeleteReportPhotoForm) form;
		int id = deletereportphotoForm.getImg_id();
		String move=null;

		try{

			DbDao.DeleteImage(id);

			Connection con=DbUtil.connect();

			String sql1 = "SELECT*FROM ImageTable WHERE img_delete=true;";
			PreparedStatement stmt = con.prepareStatement(sql1);
			ResultSet rs = stmt.executeQuery();
			List<ImageTable> imgresult = new ArrayList<ImageTable>();


			while (rs.next()) {

				if (rs.getInt("sexual") != 0 || rs.getInt("report") != 0
						|| rs.getInt("copyright") != 0
						|| rs.getInt("infringement") != 0) {
					ImageTable img = new ImageTable();
					img.setImg_id(rs.getInt("img_id"));
					img.setTitle(rs.getString("title"));
					img.setReport(rs.getInt("report"));
					img.setCopyright(rs.getInt("copyright"));
					img.setInfringement(rs.getInt("infringement"));
					img.setPortrait(rs.getInt("portrait"));
					img.setSexual(rs.getInt("sexual"));
					img.setImg_name(rs.getString("img_name"));
					imgresult.add(img);
				}
			}

			HttpSession session = request.getSession();
			session.setAttribute("repoall", imgresult);
			request.setAttribute("result","選択した写真を消去しました");
			move="success";

		}catch(Exception e){
			move="bad";
		}
		return mapping.findForward(move);
	}

}