package jp.co.phopan.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.phopan.db.DbUtil;
import jp.co.phopan.db.ImageTable;
import jp.co.phopan.form.DeleteReportPhotoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DeleteReportPhotoAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		DeleteReportPhotoForm deletereportphotoForm = (DeleteReportPhotoForm) form;

		//各変数の宣言
		int img = Integer.parseInt(request.getParameter("img_id"));
		String move=null;
		try{
			Connection con = DbUtil.connect();
			String sql="SELECT*FROM ImageTable WHERE img_id=?;";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setInt(1,img);
			ResultSet rs=stmt.executeQuery();

			ArrayList<ImageTable> imgtable = new ArrayList<ImageTable>();

			while(rs.next()) {
				ImageTable photo = new ImageTable();
				photo.setUser_id(rs.getInt("user_id"));
				photo.setImg_id(rs.getInt("img_id"));
				photo.setTitle(rs.getString("title"));
				photo.setReport(rs.getInt("report"));
				photo.setCopyright(rs.getInt("copyright"));
				photo.setInfringement(rs.getInt("infringement"));
				photo.setSexual(rs.getInt("sexual"));
				photo.setImg_name(rs.getString("img_name"));
				imgtable.add(photo);
			}
			move="success";
			request.setAttribute("deletephoto",imgtable);

			deletereportphotoForm.setImg_id(img);

		}catch(Exception e){
			//e.printStackTrace();
			move="bad";
		}
		return mapping.findForward(move);
	}
}
