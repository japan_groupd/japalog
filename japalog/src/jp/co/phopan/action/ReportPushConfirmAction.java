package jp.co.phopan.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.phopan.db.DbUtil;
import jp.co.phopan.form.ReportForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
public class ReportPushConfirmAction extends Action {
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
    	HttpServletRequest request, HttpServletResponse response) throws Exception {
    	response.setContentType("text/html; charset=UTF-8");
    	request.setCharacterEncoding("UTF-8");
    	ReportForm reportForm = (ReportForm) form;
        String report = reportForm.getReport();
        int imgid = reportForm.getImgid();
		Connection con = DbUtil.connect();
		String sql = "SELECT * FROM imagetable WHERE img_id = " + imgid + ";";
		PreparedStatement st = con.prepareStatement(sql);
		ResultSet rs = st.executeQuery();
		int reportResult = 0; //通報の数を入力する為の変数
		int total=0;

		//データベースから値を引き出すためのwhile文
		while(rs.next()){
			total=rs.getInt("copyright")+rs.getInt("portrait")+rs.getInt("infringement")+rs.getInt("sexual")+1;
			reportResult = rs.getInt(report) + 1;
		}
		sql = "UPDATE imagetable SET report="+total+"," + report + " = " + reportResult +  " WHERE img_id = " + imgid + ";";
		st = con.prepareStatement(sql);
		int rsUpdate = st.executeUpdate();
		DbUtil.disconnect(con);
		DbUtil.disstmt(st);
		return mapping.findForward("success");
    }
}
