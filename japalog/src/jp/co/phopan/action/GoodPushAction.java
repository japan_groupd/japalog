package jp.co.phopan.action;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.phopan.db.DbUtil;
import jp.co.phopan.form.PhotoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
public class GoodPushAction extends Action {
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
    	HttpServletRequest request, HttpServletResponse response) throws Exception {
    	response.setContentType("text/html; charset=UTF-8");
    	request.setCharacterEncoding("UTF-8");
    	PhotoForm photoForm = (PhotoForm) form;
		Connection con = DbUtil.connect();
		int img_id = photoForm.getImg_id();
		String sql = "SELECT total_good FROM imagetable WHERE img_id = "+ img_id + " ;";
		PreparedStatement st = con.prepareStatement(sql);
		ResultSet rs = st.executeQuery();
		int goodResult = 0;

		//データベースから値を引き出すためのwhile文
		while(rs.next()){
			goodResult = rs.getInt("total_good") + 1;
		}
		sql = "UPDATE imagetable SET total_good = " + goodResult +  " WHERE img_id = " + img_id + ";";
		st = con.prepareStatement(sql);
		int rsUpdate = st.executeUpdate();
		DbUtil.disconnect(con);
		DbUtil.disstmt(st);
		return mapping.findForward("success");
    }
}
