package jp.co.phopan.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.phopan.db.DbDao;
import jp.co.phopan.form.PhotoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class DeletePhotoAction extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

	PhotoForm photoForm = (PhotoForm)form;
	int imgid = photoForm.getImg_id();

	System.out.println(imgid);					//�m�F�p

	DbDao.DeleteImage(imgid);

	return mapping.findForward("success");
	}
}
