package jp.co.phopan.action;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.phopan.db.ConnectInfo;
import jp.co.phopan.db.DbDao;
import jp.co.phopan.db.DbUtil;
import jp.co.phopan.db.UserTable;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ProfileChangeConfirmAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		String oldname = (String)session.getAttribute("olduser_name");
		String newname = (String)session.getAttribute("newuser_name");
		String newpass = (String)session.getAttribute("newpass");
		ConnectInfo coninfo = new ConnectInfo();

		try{
			coninfo.setMy_con(DbUtil.connect());
			coninfo.setMy_stmt(DbUtil.stmt(coninfo.getMy_con()));

			//ユーザ情報更新
			DbDao.UpdateUser(coninfo.getMy_con(),oldname,newname,newpass);


			String sql = "SELECT * FROM usertable";
			coninfo.setMy_Result(coninfo.getMy_stmt().executeQuery(sql));

			while(coninfo.getMy_Result().next()){
				if(newname.equals(coninfo.getMy_Result().getString("user_name"))){
					UserTable user = new UserTable();
					user.setUser_id(coninfo.getMy_Result().getInt("user_id"));
					user.setUser_name(coninfo.getMy_Result().getString("user_name"));
					user.setPass(coninfo.getMy_Result().getString("pass"));
					session.setAttribute("loginUser",user );
					break;
				}
			}

			DbUtil.disstmt(coninfo.getMy_stmt());
			DbUtil.disconnect(coninfo.getMy_con());
		//例外処理
		}catch(SQLException e){
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}


		//遷移先へ移動
		return mapping.findForward("success");

	}
}
