package jp.co.phopan.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.phopan.db.ConnectInfo;
import jp.co.phopan.db.DbUtil;
import jp.co.phopan.db.UserTable;
import jp.co.phopan.form.ProfileForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import errormessage.ErrorMessage;

public class ProfileChangeAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProfileForm profileForm = (ProfileForm) form;	//ActionFormBean

		HttpSession session =request.getSession();
		UserTable user = (UserTable)session.getAttribute("loginUser");	//ログインしたユーザ情報

		String olduser_name = profileForm.getOlduser_name();	//元々のユーザ名
		String newuser_name = profileForm.getNewuser_name();	//新しいユーザ名
		String oldpass = profileForm.getOldpass();	//元々のパスワード
		String newpass = profileForm.getNewpass();	//新しいパスワード
		String newrekeypass = profileForm.getNewrekeypass();	//新しいパスワードの再入力
		String moveFile = "";	//遷移先

		ConnectInfo coninfo = new ConnectInfo();

		try{

			if(olduser_name.equals(user.getUser_name()) && oldpass.equals(user.getPass())){

				if(newpass.equals(newrekeypass)){
					coninfo.setMy_con(DbUtil.connect());
					coninfo.setMy_stmt(DbUtil.stmt(coninfo.getMy_con()));

					String sql = "SELECT * FROM usertable";
					coninfo.setMy_Result(coninfo.getMy_stmt().executeQuery(sql));
					while(coninfo.getMy_Result().next()){
						if(newuser_name.equals(coninfo.getMy_Result().getString("user_name")) || newpass.equals(coninfo.getMy_Result().getString("pass"))){
							//遷移先へ送る
							request.setAttribute("passerror", ErrorMessage.MATCH_PASS_OR_NAME);

							//遷移先を指定
							moveFile = "failed";
							break;
						}
					}


					DbUtil.disstmt(coninfo.getMy_stmt());
					DbUtil.disconnect(coninfo.getMy_con());
				}
				else{
					//遷移先へ送る
					request.setAttribute("passerror", ErrorMessage.NO_MATCH_REKEYPASS);

					//遷移先を指定
					moveFile = "failed";
				}
			}
			else{
				//遷移先へ送る
				request.setAttribute("passerror", ErrorMessage.INCORRECT_USER);

				//遷移先を指定
				moveFile = "failed";
			}
			if(moveFile.equals("") || moveFile== null){

				//遷移先へ送る
				session.setAttribute("olduser_name",olduser_name );
				session.setAttribute("newuser_name", newuser_name);
				session.setAttribute("oldpass",oldpass );
				session.setAttribute("newpass", newpass);
				session.setAttribute("newrekeypass", newrekeypass);

				//遷移先を指定
				moveFile = "success";
			}

		//例外処理
		}catch(Exception e){

			//遷移先を指定
			moveFile = "failed";

			e.printStackTrace();
		}

		//遷移先へ移動
		return mapping.findForward(moveFile);

	}
}
