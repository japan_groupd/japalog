
package jp.co.phopan.action;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.phopan.db.DbDao;
import jp.co.phopan.db.DbUtil;
import jp.co.phopan.db.UserTable;
import jp.co.phopan.form.LoginForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class NewAccoutConfirmAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginForm loginForm = (LoginForm) form;	//ActionFormBean

		String name = loginForm.getUser_name();
		String pass = loginForm.getPass();

		//例外処理開始
		try{

			//Connection接続
			Connection con=DbUtil.connect();

			//ユーザ登録
			DbDao.InsertUser(con,name, pass);


			String sql = "SELECT * FROM usertable ORDER BY user_id DESC";
			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery(sql);
			HttpSession session = request.getSession();
			while(rs.next()){
				UserTable user = new UserTable();
				user.setUser_id(rs.getInt("user_id"));
				user.setUser_name(rs.getString("user_name"));
				user.setPass(rs.getString("pass"));

				session.setAttribute("loginUser",user );
				break;
			}

			//Statement切断
			DbUtil.disconnect(con);

		//例外処理
		}catch(SQLException e){
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}

		//遷移先へ移動
		return mapping.findForward("success");
	}
}


