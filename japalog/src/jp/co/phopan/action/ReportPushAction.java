package jp.co.phopan.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.phopan.form.ReportForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReportPushAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
    	HttpServletRequest request, HttpServletResponse response) throws Exception {
        ReportForm reportForm = (ReportForm) form;
        String report = reportForm.getReport();
        int imgid = reportForm.getImgid();

        request.setAttribute("report",report);
        request.setAttribute("imgid",imgid);
        return mapping.findForward("success");
    }
}
