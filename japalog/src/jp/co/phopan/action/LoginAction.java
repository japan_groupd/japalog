package jp.co.phopan.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.phopan.db.DbUtil;
import jp.co.phopan.db.ImageTable;
import jp.co.phopan.db.UserTable;
import jp.co.phopan.form.LoginForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import errormessage.ErrorMessage;

public class LoginAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		request.setCharacterEncoding("UTF-8");
		LoginForm loginForm = (LoginForm) form;

		String moveFile = null;		//移動先ファイル

		//loginformからのデータを取得
		String str_inputusername = loginForm.getUser_name();
		String inputPass = loginForm.getPass();

		try{
			Connection con=DbUtil.connect();						//Connection接続

			if(str_inputusername.equals("administer") && inputPass.equals("010101")){
				String sql1 = "SELECT*FROM ImageTable WHERE img_delete=true;";
				PreparedStatement stmt = con.prepareStatement(sql1);
				ResultSet rs = stmt.executeQuery();
				List<ImageTable> imgresult = new ArrayList<ImageTable>();


				while (rs.next()) {

					if (rs.getInt("sexual") != 0 || rs.getInt("report") != 0
							|| rs.getInt("copyright") != 0
							|| rs.getInt("infringement") != 0) {
						ImageTable img = new ImageTable();
						img.setImg_id(rs.getInt("img_id"));
						img.setTitle(rs.getString("title"));
						img.setReport(rs.getInt("report"));
						img.setCopyright(rs.getInt("copyright"));
						img.setInfringement(rs.getInt("infringement"));
						img.setPortrait(rs.getInt("portrait"));
						img.setSexual(rs.getInt("sexual"));
						img.setImg_name(rs.getString("img_name"));
						imgresult.add(img);
					}
				}
				HttpSession session = request.getSession();
				session.setAttribute("repoall", imgresult);
				moveFile="adomi";

			}
			else{
				if((!str_inputusername.equals("") || !inputPass.equals(""))){
					//入力値がnullや空文字ではない時
					String sql="SELECT*FROM usertable;";

					PreparedStatement stmt = con.prepareStatement(sql);

					ResultSet rs = stmt.executeQuery();

				if(serchUser(str_inputusername,rs,request,inputPass)){
					HttpSession se = request.getSession();
					UserTable session=(UserTable) se.getAttribute("loginUser");
					String sql1 = "SELECT * FROM usertable WHERE user_id = ?";
					stmt=con.prepareStatement(sql1);
					stmt.setInt(1,session.getUser_id());
					rs = stmt.executeQuery();
					while(rs.next()){
						if(rs.getBoolean("user_delete")){
							moveFile="success";
						}
						else{
							request.setAttribute("error", ErrorMessage.DELETEUSER);
							moveFile = "failed";
						}
					}

				}
				else{
					moveFile = "failed";
					request.setAttribute("error", ErrorMessage.INCORRECT_USER);
				}

			}
		}
			DbUtil.disconnect(con);					//Statement切断

		}catch(NumberFormatException ex){

			ex.printStackTrace();
			//入力値チェック
			moveFile = "failed";

		}catch(Exception ex){
			ex.printStackTrace();
		}

		if(moveFile == null){
			moveFile = "failed";
			request.setAttribute("error", ErrorMessage.INCORRECT_USER);
		}
		return mapping.findForward(moveFile);
	}

	public static boolean serchUser(String user_name,ResultSet rs,HttpServletRequest request,String inputPass)throws SQLException{
		HttpSession session = request.getSession();
		UserTable user = new UserTable();
		boolean flag=false;
		while(rs.next()) {
				if(user_name.equals(rs.getString("user_name"))&&inputPass.equals(rs.getString("pass"))){
					//合致したら抜ける
					user.setUser_id(rs.getInt("user_id"));
					user.setUser_name(rs.getString("user_name"));
					user.setPass(rs.getString("pass"));

					session.setAttribute("loginUser",user );
					flag=true;
					break;
			}
		}
		return flag;
	}
}

