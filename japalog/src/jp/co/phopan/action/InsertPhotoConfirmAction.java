package jp.co.phopan.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.phopan.db.DbDao;
import jp.co.phopan.db.UserTable;
import jp.co.phopan.form.PhotoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class InsertPhotoConfirmAction extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

	PhotoForm photoForm = (PhotoForm)form;
	String imgname = photoForm.getImg_name();
	String title = photoForm.getTitle();
	int category_id =photoForm.getCategory_id();
	String details=photoForm.getDetails();

	HttpSession session = request.getSession(true);
	UserTable user = (UserTable)session.getAttribute("loginUser");

	String move="bad";

	DbDao.InsertImage(user.getUser_id(), imgname, category_id, title, details);


	move="success";

	return mapping.findForward(move);
	}
}
