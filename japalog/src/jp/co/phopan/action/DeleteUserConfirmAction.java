package jp.co.phopan.action;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.phopan.db.DbUtil;
import jp.co.phopan.form.DeleteUserForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DeleteUserConfirmAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		DeleteUserForm deleteuserForm = (DeleteUserForm) form;

		//各変数の宣言
		int id = deleteuserForm.getUser_id();

		String move;

		try{

			Connection con =DbUtil.connect();

			String sql ="UPDATE usertable set user_delete=false where user_id=?";
			PreparedStatement stmt;

			stmt = con.prepareStatement(sql);

			stmt.setInt(1, id);

			stmt.executeUpdate();

			//接続を切断
			stmt.close();
			DbUtil.disconnect(con);

			move="success";
			request.setAttribute("result","選択したユーザを消去しました");

		}catch(Exception e){
			move="bad";
		}
		return mapping.findForward(move);
	}
}
