package jp.co.phopan.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.phopan.db.DbUtil;
import jp.co.phopan.form.LoginForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import errormessage.ErrorMessage;

public class NewAccountAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		LoginForm loginForm = (LoginForm) form;	//ActionFormBean

		String name = loginForm.getUser_name();	//ユーザ名
		String pass = loginForm.getPass();	//パスワード
		String rekeypass = loginForm.getRekeypass();	//パスワード再入力
		String consent = loginForm.getConsent();	//利用規約の同意
		String moveFile = null;	//遷移先

		try{

			Connection con=DbUtil.connect();						//Connection接続
			String sql="SELECT*FROM UserTable;";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				//nameとpassが存在した時の処理
				if(name.equals(rs.getString("user_name"))||pass.equals(rs.getString("pass"))){
					//遷移先へ送る
					request.setAttribute("duplicationerror",ErrorMessage.MATCH_PASS_OR_NAME);
					moveFile="failed";
					break;

				}else{
					//パスワードと再入力が一致
					if(pass.equals(rekeypass)){

						//利用規約に同意
						if(consent.equals("同意します")){

							//遷移先へ送る
							request.setAttribute("user_name",name);
							request.setAttribute("pass",pass);
							request.setAttribute("rekeypass",rekeypass);

							//遷移先を指定
							moveFile = "success";
						}else{

							//遷移先へ送る
							request.setAttribute("consenterror",ErrorMessage.NO_CONSENT_AGREEMENT);

							//遷移先を指定
							moveFile = "failed";
						}
					}else {

						//遷移先へ送る
						request.setAttribute("passerror",ErrorMessage.NO_MATCH_REKEYPASS);

						//遷移先を指定
						moveFile = "failed";
					}
				}
			}
			//例外処理
		}catch(Exception e){

			//遷移先を指定
			moveFile = "failed";
			e.printStackTrace();
		}
		//遷移先へ移動
		return mapping.findForward(moveFile);
	}
}
