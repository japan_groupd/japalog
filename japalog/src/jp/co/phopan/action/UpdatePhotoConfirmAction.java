package jp.co.phopan.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.phopan.db.DbDao;
import jp.co.phopan.form.PhotoForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class UpdatePhotoConfirmAction extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

	HttpSession session = request.getSession(false);
	PhotoForm photoForm = (PhotoForm)form;
	String imgname = photoForm.getImg_name();
	String title = photoForm.getTitle();
	int category_id =photoForm.getCategory_id();
	String details=photoForm.getDetails();

	//int imgid = Integer.parseInt((String)session.getAttribute("img_id"));
	int imgid = (Integer)session.getAttribute("img_id");

	String move="success";

	DbDao.UpdateImage(imgid, category_id, title, details);




	return mapping.findForward(move);
	}
}
