package jp.co.phopan.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.phopan.form.DeleteUserForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DeleteUserAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		DeleteUserForm deleteuserForm = (DeleteUserForm) form;

		//各変数の宣言
		/*
		int id = deleteuserForm.getUser_id();
		String name = deleteuserForm.getUser_name();
		String pass = deleteuserForm.getPass();
		*/

		int id = Integer.parseInt(request.getParameter("user_id"));
		String name = request.getParameter("user_name");
		String pass = request.getParameter("pass");

		String move;

		try{
			request.setAttribute("id",id+"");
			request.setAttribute("name",name);
			request.setAttribute("pass",pass);

			deleteuserForm.setUser_id(id);
			deleteuserForm.setUser_name(name);
			deleteuserForm.setPass(pass);

			move="success";
		}catch(Exception e){
			move="bad";
		}
		return mapping.findForward(move);

	}
}