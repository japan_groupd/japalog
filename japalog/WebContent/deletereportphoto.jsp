<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="jp.co.phopan.db.DbUtil"%>
<%@page import="java.sql.Connection"%>
<%@page import="jp.co.phopan.db.ImageTable"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	String deleteresult = (String) request.getAttribute("result");
	String err = (String) request.getAttribute("error");
	ArrayList<ImageTable> imgAry = (ArrayList<ImageTable>) session.getAttribute("repoall");
%>

<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="css/adminbase.css" rel="stylesheet">
<script type="text/javascript" src="js/main.js"></script>
<title>phopan</title>
</head>
<body>
	<div id="basesize">

			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/headeradmin.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
		<!--  ▼ワッパー -->
		<div id="wrapper">
			<!--  ▽メニュー -->
			<!--  △メニュー -->
			<div id="main">

				<h3>管理画面</h3>
				<br>
				<table class="posi-center">
					<tr>
						<td><h1>通報写真削除</h1></td>
					<tr>
					<tr><%if(deleteresult!=null){ %>
						<td><font color="red"><%=deleteresult%></font></td>
						<%} %>
					<tr>
						<td>消したい写真を削除</td>
					</tr>
				</table>
				<%if (err != null) {%>
					<font color="red"><%=err%></font>
				<%}%>


				<table class="posi-ccenter">
						<tr>
							<td>
								<table border="1" class="admin posi-center">
									<tr>
										<th class="id">ID</th>
										<th class="title">タイトル</th>
										<th class="report">通報数</th>
										<th class="report">著作権</th>
										<th class="report">肖像権</th>
										<th class="report">人権侵害</th>
										<th class="report">不適切</th>
										<th class="photo">写真</th>
									</tr>

									<%for(ImageTable it :imgAry) {%>
										<tr>
											<td><%=it.getImg_id() %></td>
											<td><%=it.getTitle() %></td>
											<td><%=it.getReport() %></td>
											<td><%=it.getCopyright() %></td>
											<td><%=it.getPortrait() %></td>
											<td><%=it.getInfringement() %></td>
											<td><%=it.getSexual() %></td>
											<td><img src ='contents/<%= it.getImg_name() %>' class="admiimg"></td>
											<html:form action="/deleteReportPhoto" method="POST">

											<input type="hidden" name="img_id" value="<%=it.getImg_id()%>">
											<td class="delete"><html:submit value="削除" styleClass="button" /></td></html:form>
										</tr>
									<%} %>
								</table>
							</td>
						</tr>
				</table>
			</div>
		</div>
		<!--  ▲ワッパー -->


		<!--  ▼フッター -->
		<div id="footer">
			<a href="#basesize">ページ上部に戻る</a>
		</div>
		<!--  ▲フッター -->

	</div>
</body>
</html:html>