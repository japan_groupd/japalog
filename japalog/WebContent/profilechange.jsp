<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<% String nameerror = (String)request.getAttribute("nameerror"); %>
<% String passerror = (String)request.getAttribute("passerror"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>photon</title>
		<script type="text/javascript" src="js/calendar.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<link type="text/css" href="css/base.css" rel="stylesheet">
</head>
<body>
<div id="basesize">
			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
			<jsp:include page="/search.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
			<!--  ▼ワッパー -->
			<div id="wrapper">
				<div id="main" class="column">
				<html:form action="/profileChange">
					<table class="posi-center">

						<tr>
							<td><h2>ユーザ情報変更</h2></td>
						</tr>
						<tr>
							<td><font color="red"><% if(passerror!=null){out.println(passerror);} if(nameerror!=null){out.println(nameerror);} %><html:errors/></font></td>
						</tr>
						<tr>
							<td><h6>ユーザ名とパスワードを入力して下さい</h6></td>
						</tr>
						<tr>
							<td class="posi-left"><font color ="red">※</font>変更前のユーザ名</td>
						</tr>
						<tr>
							<td><html:text property="olduser_name" /></td>
						</tr>
						<tr>
							<td class="posi-left"><font color ="red">※</font>変更後のユーザ名(1文字以上10文字以下)</td>
						</tr>
						<tr>
							<td><html:text property="newuser_name" /></td>
						</tr>
						<tr>
							<td class="posi-left"><font color ="red">※</font>変更前のパスワード</td>
						</tr>
						<tr>
							<td><html:password property="oldpass"/></td>
						</tr>
						<tr>
							<td class="posi-left"><font color ="red">※</font>変更後のパスワード(6字以上14字以下)</td>
						</tr>
						<tr>
							<td><html:password property="newpass"/></td>
						</tr>
						<tr>
							<td class="posi-left"><font color ="red">※</font>変更後のパスワード(再入力)</td>
						</tr>
						<tr>
							<td><html:password property="newrekeypass" /></td>
						</tr>
						<tr>
							<td><input type="submit" value="確認" class="button"><input type="button" class="button" onClick="document.location='mypage.jsp';" value="戻る"/></td>
						</tr>
					</table>
					</html:form>
				</div>
			</div>
			<!--  ▲ワッパー -->
			<!--  ▼フッター -->
			<div id="footer">
				<a href="#basesize">ページ上部に戻る</a>
			</div>
			<!--  ▲フッター -->
		</div>
</body>
</html:html>