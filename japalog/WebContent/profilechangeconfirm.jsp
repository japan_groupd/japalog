<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>photon</title>
		<script type="text/javascript" src="js/calendar.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<link type="text/css" href="css/base.css" rel="stylesheet">
</head>
<body>
<div id="basesize">
			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
			<jsp:include page="/search.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
			<!--  ▼ワッパー -->
			<div id="wrapper">
				<div id="main" class="column">
					<html:form action="/profilechangeConfirm">
						<table class="posi-center">
							<tr>
							<td><h2>ユーザ情報変更</h2></td>
							</tr>
							<tr>
								<td class="posi-left"><h6>こちらでよろしいですか？</h6></td>
							</tr>
							<tr>
								<td class="posi-left">ユーザ名</td>
							</tr>
							<tr>
								<td><html:text property="newuser_name" disabled="true"/></td>
								<html:hidden property="olduser_name"></html:hidden>
								<html:hidden property="newuser_name"></html:hidden>
							</tr>
							<tr>
								<td class="posi-left">パスワード</td>
							</tr>
							<tr>
								<td><html:password property="newpass" disabled="true"/></td>
								<html:hidden property="oldpass"></html:hidden>
								<html:hidden property="newpass"></html:hidden>
								<html:hidden property="newrekeypass"></html:hidden>
							</tr>
							<tr>
								<td><html:submit value="変更" styleClass="button" />
								<input type="button" class="button" onClick="document.location='profilechange.jsp';" value="戻る"></td>
							</tr>
					</table>
					</html:form>
				</div>
			</div>
			<!--  ▲ワッパー -->
			<!--  ▼フッター -->
			<div id="footer">
				<a href="#basesize">ページ上部に戻る</a>
			</div>
			<!--  ▲フッター -->
		</div>
</body>
</html:html>