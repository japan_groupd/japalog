<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%String filename =(String)request.getAttribute("fileName");%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
		<title>phopan</title>
		<script type="text/javascript" src="js/calendar.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<link type="text/css" href="css/base.css" rel="stylesheet">
	</head>
	<body>
		<div id="basesize">
		<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
			<jsp:include page="/search.jsp" flush="true" />
		<!--  ▲ヘッダー＆カテゴリ -->
		<!--  ▼ワッパー -->
			<div id="wrapper">
				<div id="main" class="column">
					<table class="posi-center">
						<tr>
							<td class="posi-center"><h2>写真を投稿しました</h2></td>
						</tr>
						<tr>
							<td class="posi-center">
								<form action="mypage.jsp">
									<input type="submit"  class="button" value="マイページへ">
								</form>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<!--  â²ã¯ããã¼ -->
			<!--  â¼ããã¿ã¼ -->
			<div id="footer">
				<a href="#basesize">ページ上部に戻る</a>
			</div>
			<!--  â²ããã¿ã¼ -->
		</div>
	</body>
</html>