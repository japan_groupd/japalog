<%@page import="java.util.ArrayList"%>
<%@page import="jp.co.phopan.db.UserTable"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<% String id =   (String)request.getAttribute("id");%>
<% String name = (String)request.getAttribute("name"); %>
<% String pass = (String)request.getAttribute("pass"); %>

<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="css/adminbase.css" rel="stylesheet">
<script type="text/javascript" src="js/main.js"></script>
<title>phopan</title>
</head>
<body>
	<div id="basesize">

			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/headeradmin.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
		<!--  ▼ワッパー -->
		<div id="wrapper">
			<div id="main" class="column">
				<h3>管理画面</h3>
				<br>
				<table class="admin-mini posi-center">
					<tr>
						<td>
							<h1>ユーザ削除</h1>
						</td>
					</tr>
					<tr>
						<td>このユーザを削除しますか?</td>
					</tr>
				</table>
				<table>
				<html:form action="/deleteUserConfirm">
					<tr>
						<td>
							<table border="1" class="admin-mini posi-center">
									<tr>
										<th class="id">ID</th>
										<th>ユーザ名</th>
										<th class="pw">パスワード</th>
									</tr>

									<tr>
										<td><%=id%></td>
										<td><%=name%></td>
										<td><%=pass%></td>
									</tr>
										<html:hidden property="user_id"/>
										<html:hidden property="user_name"/>
										<html:hidden property="pass"/>

							</table>
						</td>
					</tr>
					<tr>
						<td>
							<html:submit property="delete" value="削除" styleClass="button" />
							<input type="button" class="button" onClick="document.location='deleteuser.jsp';" value="戻る">
						</td>
					</tr>
					</html:form>
				</table>
			</div>
			<!--  ▲ワッパー -->
			<!--  ▼フッター -->
			<div id="footer">
				<a href="#basesize">ページ上部に戻る</a>
			</div>
			<!--  ▲フッター -->
		</div>
	</div>
</body>
</html:html>