<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%String yes = "同意します"; %>
<%String no = "同意しません"; %>
<% String passerror = (String)request.getAttribute("passerror"); %>
<% String consenterror = (String)request.getAttribute("consenterror"); %>
<% String duplicationerror = (String)request.getAttribute("duplicationerror"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>phopan</title>
	<script type="text/javascript" src="js/main.js"></script>
	<link type="text/css" href="css/base.css" rel="stylesheet">
</head>
<body>
<div id="basesize">
	<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
	<!--  ▲ヘッダー＆カテゴリ -->


	<!--  ▼ワッパー -->
	<div id="wrapper">
		<!--  ▽メイン -->
		<div id="main" class="posi-center">
		<html:form action="/newAccount"  method="POST" enctype="multipart/form-data">

		<font color="red">
						<% if(passerror!=null){%><%=passerror %><% } %><% if(consenterror!=null){%><%=consenterror %><% } %>
						<% if(duplicationerror!=null){%><%=duplicationerror %><% } %>
		<html:errors/></font>

			<table class="edit posi-center">
					<tr>
						<td colspan="2"><h2>新規ユーザ登録</h2></td>
					</tr>
					<tr>
						<td  colspan="2"><h6>以下の必須項目を入力して下さい</h6></td>
					</tr>
					<tr>
						<td  colspan="2" class="posi-left" ><font color ="red">※</font>ユーザ名(1文字以上10文字以下)</td>
					</tr>
					<tr>
						<td  colspan="2"><html:text property="user_name" /><br></td>
					</tr>
					<tr>
						<td  colspan="2" class="posi-left"><font color ="red">※</font>パスワード(6字以上14字以下)</td>
					</tr>
					<tr>
						<td colspan="2"><html:password property="pass"/><br></td>
					</tr>
					<tr>
						<td  colspan="2" class="posi-left"><font color ="red">※</font>パスワード再入力</td>
					</tr>
					<tr>
						<td  colspan="2"><html:password property="rekeypass" /><br></td>
					</tr>
					<tr>
						<td  colspan="2"><h2><html:link page="/agreement.jsp">利用規約詳細</html:link></h2></td>
					</tr>
					<tr>
						<td>
							<p><input class="radio" type="radio" name="consent" value=<%=yes %>>同意します</p>
						</td>
						<td>
							<p ><input class="radio" type="radio" name="consent" value=<%=no %> checked>同意しません</p>
						</td>
					</tr>
					<tr>
						<td>
							<html:submit property="submit" styleClass="button" value="確認"/>
						</td>
						<td>
							<input type="button" class="button" onClick="document.location='index.jsp';" value="戻る"/>
						</td>
					</tr>
				</table>
			</html:form>
		</div>
		<!--  △メイン -->
	</div>
	<!--  ▲ワッパー -->


	<!--  ▼フッター -->
	<div id="footer">
		<a href="#">ページ上部に戻る</a>
	</div>
	<!--  ▲フッター -->

</div>
</body>
</html:html>