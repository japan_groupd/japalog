<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<% String imgerror = (String)request.getAttribute("imgerror"); %>
<% String titleerror = (String)request.getAttribute("titleerror"); %>
<% String deterror = (String)request.getAttribute("deterror"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>phopan</title>
		<script type="text/javascript" src="js/calendar.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<link type="text/css" href="css/base.css" rel="stylesheet">
	</head>
	<script language="JavaScript"><!--
		function setData(param1,param2)
		{
			var start = document.photoForm.details.selectionStart;	//選択開始位置を取得
			var end = document.photoForm.details.selectionEnd;	//選択最後位置を取得
			var str = document.photoForm.details.value.slice(start,end);	//選択開始位置から選択最後位置までの文字列を取得
			document.photoForm.details.value = document.photoForm.details.value.substr(0,start) + param1 + str + param2 + document.photoForm.details.value.substr(end,document.photoForm.details.value.length);
		}
		// -->
	</script>
	<body>
		<div id="basesize">
		<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
			<jsp:include page="/search.jsp" flush="true" />
		<!--  ▲ヘッダー＆カテゴリ -->
		<!--  ▼ワッパー -->
		<div id="wrapper">
			<div id="main">
				<h2  class="posi-center">写真投稿</h2>
				<h6 class="posi-center">写真を追加してください</h6>
						<font color="red">
							<% if(imgerror!=null){%><%=imgerror %><br><% } %>
							<% if(titleerror!=null){%><%=titleerror %><br><% } %>
							<% if(deterror!=null){%><%=deterror %><br><% } %>
						</font>
				<html:form action = "/insertPhoto" method="POST" enctype="multipart/form-data">
					<table  class="posi-center" >
						<tr>
							<td colspan="3"><h6>投稿写真</h6></td>
						</tr>
						<tr>
							<td colspan="3"><html:file property="fileup" accept="image/*; capture=camera"></html:file></td>
						</tr>
						<tr>
							<td colspan="3"><h6>カテゴリ</h6></td>
						</tr>
						<tr>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="1">風景・自然</p>
							</td>
							<td>
								<p ><input class="radio" type="radio" name="category_id" value="2">人物</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="3">乗り物</p>
							</td>
						</tr>
						<tr>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="4" >動物</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="5">イラストアート</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="6">花・植物</p>
							</td>
						</tr>
						<tr>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="7">建物・町並み</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="8">グルメ</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="9">イベント</p>
							</td>
						</tr>
						<tr>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="10">インテリア</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="11">スポーツ</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="12" checked>その他</p>
							</td>
						</tr>
						<tr>
							<td colspan="3"><h6>写真タイトル(1～13文字)</h6></td>
						</tr>
						<tr>
							<td colspan="3"><html:text property="title" ></html:text></td>
						</tr>
						<tr>
							<td colspan="3"><h6>詳細(1～200文字)</h6>
								<input type="button" class="button" value="太字" onClick="setData('<b>','</b>')">
								<input type="button" class="button" value="斜体" onClick="setData('<i>','</i>')">
								<input type="button" class="button" value="打ち消し" onClick="setData('<s>','</s>')">
								<input type="button" class="button" value="赤色" onClick="setData('<font color=&quot#ff0000&quot>','</font>')">
								<input type="button" class="button" value="緑色" onClick="setData('<font color=&quot#00ff00&quot>','</font>')">
								<input type="button" class="button" value="青色" onClick="setData('<font color=&quot#0000ff&quot>','</font>')"><BR>
								<textarea  class="ins" name="details"></textarea>
							</td>
						</tr>
					</table>
					<table class="posi-center"><tr><td>
					<input type="submit" class="button" value="確認">
					<!--  style="position: absolute; left: 40%;" //めも -->
					<input type="button" class="button" onClick="document.location='mypage.jsp';" value="戻る">
					</td></tr></table>
				</html:form>
			</div>
		</div>
		<!--  ▲ワッパー -->
		<!--  ▼フッター -->
		<div id="footer">
			<a href="#basesize">ページ上部に戻る</a>
		</div>
		<!--  ▲フッター -->
		</div>
	</body>
</html:html>