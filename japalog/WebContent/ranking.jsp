<%@ page language="java" import="java.util.*" import="jp.co.phopan.db.ContentsTable" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page import = "java.sql.Connection" %>
<%@ page import = "jp.co.phopan.db.DbUtil" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.sql.ResultSet" %>
<%@ page import = "java.sql.Connection" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.text.DateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<html:html>
<body>
 <%
    Connection con = DbUtil.connect();
	String sql = "SELECT r1.img_id,r1.total_good,r1.img_name,(SELECT count(r2.total_good) FROM imagetable as r2 WHERE r2.total_good > r1.total_good) + 1 as rank FROM imagetable as r1 WHERE img_delete=true ORDER BY r1.total_good DESC LIMIT 3;";
	PreparedStatement st = con.prepareStatement(sql);
	ResultSet rs = st.executeQuery();
	int reportResult = 0; //通報の数を入力する為の変数
	//各レコードを入力するためのArrayList
	ArrayList<Integer> arrayId = new ArrayList<Integer>();
	ArrayList<String> arrayName = new ArrayList<String>();
	ArrayList<Integer> arrayGood = new ArrayList<Integer>();
	ArrayList<Integer> arrayId2 = new ArrayList<Integer>();
	ArrayList<String> arrayName2 = new ArrayList<String>();
	ArrayList<Integer> arrayGood2 = new ArrayList<Integer>();
	ArrayList<Integer> arrayId3 = new ArrayList<Integer>();
	ArrayList<String> arrayName3 = new ArrayList<String>();
	ArrayList<Integer> arrayGood3 = new ArrayList<Integer>();

	//データベースから値を引き出すためのwhile文
	while(rs.next()){
		arrayId.add(rs.getInt("img_id"));
		arrayName.add(rs.getString("img_name"));
		arrayGood.add(rs.getInt("total_good"));
	}

	//時刻を変数strに入力
	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
	String str =sdf1.format(new Date());
	sql = "SELECT r1.img_id,r1.total_good,r1.img_name,(SELECT count(r2.total_good) FROM imagetable as r2 ) + 1 as rank FROM imagetable as r1 WHERE uploadtime ='"+str+"' AND img_delete=true  ORDER BY r1.total_good DESC LIMIT 3 ;";
	st = con.prepareStatement(sql);
	ResultSet rs2 = st.executeQuery();

	//データベースから値を引き出すためのwhile文
	while(rs2.next()){
		arrayId2.add(rs2.getInt("img_id"));
		arrayName2.add(rs2.getString("img_name"));
		arrayGood2.add(rs2.getInt("total_good"));
	}
	String yearmonth = str.substring(0,8);
	String day = str.substring(8,10);
	String week = "";

	//日付によって第1週から4週のどれかを指定するためのif文
	if(day.equals("01")){
		week = "'" + yearmonth + "01' OR uploadtime = '" + yearmonth + "02' OR uploadtime = '" + yearmonth + "03' OR uploadtime = '" + yearmonth + "04' OR uploadtime = '" + yearmonth + "05' OR uploadtime = '" + yearmonth + "06' OR uploadtime = '" + yearmonth + "07'";
	}else if(day.equals("02")){
		week = "'" + yearmonth + "01' OR uploadtime = '" + yearmonth + "02' OR uploadtime = '" + yearmonth + "03' OR uploadtime = '" + yearmonth + "04' OR uploadtime = '" + yearmonth + "05' OR uploadtime = '" + yearmonth + "06' OR uploadtime = '" + yearmonth + "07'";
	}else if(day.equals("03")){
		week = "'" + yearmonth + "01' OR uploadtime = '" + yearmonth + "02' OR uploadtime = '" + yearmonth + "03' OR uploadtime = '" + yearmonth + "04' OR uploadtime = '" + yearmonth + "05' OR uploadtime = '" + yearmonth + "06' OR uploadtime = '" + yearmonth + "07'";
	}else if(day.equals("04")){
		week = "'" + yearmonth + "01' OR uploadtime = '" + yearmonth + "02' OR uploadtime = '" + yearmonth + "03' OR uploadtime = '" + yearmonth + "04' OR uploadtime = '" + yearmonth + "05' OR uploadtime = '" + yearmonth + "06' OR uploadtime = '" + yearmonth + "07'";
	}else if(day.equals("05")){
		week = "'" + yearmonth + "01' OR uploadtime = '" + yearmonth + "02' OR uploadtime = '" + yearmonth + "03' OR uploadtime = '" + yearmonth + "04' OR uploadtime = '" + yearmonth + "05' OR uploadtime = '" + yearmonth + "06' OR uploadtime = '" + yearmonth + "07'";
	}else if(day.equals("06")){
		week = "'" + yearmonth + "01' OR uploadtime = '" + yearmonth + "02' OR uploadtime = '" + yearmonth + "03' OR uploadtime = '" + yearmonth + "04' OR uploadtime = '" + yearmonth + "05' OR uploadtime = '" + yearmonth + "06' OR uploadtime = '" + yearmonth + "07'";
	}else if(day.equals("07")){
		week = "'" + yearmonth + "01' OR uploadtime = '" + yearmonth + "02' OR uploadtime = '" + yearmonth + "03' OR uploadtime = '" + yearmonth + "04' OR uploadtime = '" + yearmonth + "05' OR uploadtime = '" + yearmonth + "06' OR uploadtime = '" + yearmonth + "07'";
	}else if(day.equals("08")){
		week = "'" + yearmonth + "08' OR uploadtime = '" + yearmonth + "09' OR uploadtime = '" + yearmonth + "10' OR uploadtime = '" + yearmonth + "11' OR uploadtime = '" + yearmonth + "12' OR uploadtime = '" + yearmonth + "13' OR uploadtime = '" + yearmonth + "14'";
	}else if(day.equals("09")){
		week = "'" + yearmonth + "08' OR uploadtime = '" + yearmonth + "09' OR uploadtime = '" + yearmonth + "10' OR uploadtime = '" + yearmonth + "11' OR uploadtime = '" + yearmonth + "12' OR uploadtime = '" + yearmonth + "13' OR uploadtime = '" + yearmonth + "14'";
	}else if(day.equals("10")){
		week = "'" + yearmonth + "08' OR uploadtime = '" + yearmonth + "09' OR uploadtime = '" + yearmonth + "10' OR uploadtime = '" + yearmonth + "11' OR uploadtime = '" + yearmonth + "12' OR uploadtime = '" + yearmonth + "13' OR uploadtime = '" + yearmonth + "14'";
	}else if(day.equals("11")){
		week = "'" + yearmonth + "08' OR uploadtime = '" + yearmonth + "09' OR uploadtime = '" + yearmonth + "10' OR uploadtime = '" + yearmonth + "11' OR uploadtime = '" + yearmonth + "12' OR uploadtime = '" + yearmonth + "13' OR uploadtime = '" + yearmonth + "14'";
	}else if(day.equals("12")){
		week = "'" + yearmonth + "08' OR uploadtime = '" + yearmonth + "09' OR uploadtime = '" + yearmonth + "10' OR uploadtime = '" + yearmonth + "11' OR uploadtime = '" + yearmonth + "12' OR uploadtime = '" + yearmonth + "13' OR uploadtime = '" + yearmonth + "14'";
	}else if(day.equals("13")){
		week = "'" + yearmonth + "08' OR uploadtime = '" + yearmonth + "09' OR uploadtime = '" + yearmonth + "10' OR uploadtime = '" + yearmonth + "11' OR uploadtime = '" + yearmonth + "12' OR uploadtime = '" + yearmonth + "13' OR uploadtime = '" + yearmonth + "14'";
	}else if(day.equals("14")){
		week = "'" + yearmonth + "08' OR uploadtime = '" + yearmonth + "09' OR uploadtime = '" + yearmonth + "10' OR uploadtime = '" + yearmonth + "11' OR uploadtime = '" + yearmonth + "12' OR uploadtime = '" + yearmonth + "13' OR uploadtime = '" + yearmonth + "14'";
	}else if(day.equals("15")){
		week = "'" + yearmonth + "15' OR uploadtime = '" + yearmonth + "16' OR uploadtime = '" + yearmonth + "17' OR uploadtime = '" + yearmonth + "18' OR uploadtime = '" + yearmonth + "19' OR uploadtime = '" + yearmonth + "20' OR uploadtime = '" + yearmonth + "21'";
	}else if(day.equals("16")){
		week = "'" + yearmonth + "15' OR uploadtime = '" + yearmonth + "16' OR uploadtime = '" + yearmonth + "17' OR uploadtime = '" + yearmonth + "18' OR uploadtime = '" + yearmonth + "19' OR uploadtime = '" + yearmonth + "20' OR uploadtime = '" + yearmonth + "21'";
	}else if(day.equals("17")){
		week = "'" + yearmonth + "15' OR uploadtime = '" + yearmonth + "16' OR uploadtime = '" + yearmonth + "17' OR uploadtime = '" + yearmonth + "18' OR uploadtime = '" + yearmonth + "19' OR uploadtime = '" + yearmonth + "20' OR uploadtime = '" + yearmonth + "21'";
	}else if(day.equals("18")){
		week = "'" + yearmonth + "15' OR uploadtime = '" + yearmonth + "16' OR uploadtime = '" + yearmonth + "17' OR uploadtime = '" + yearmonth + "18' OR uploadtime = '" + yearmonth + "19' OR uploadtime = '" + yearmonth + "20' OR uploadtime = '" + yearmonth + "21'";
	}else if(day.equals("19")){
		week = "'" + yearmonth + "15' OR uploadtime = '" + yearmonth + "16' OR uploadtime = '" + yearmonth + "17' OR uploadtime = '" + yearmonth + "18' OR uploadtime = '" + yearmonth + "19' OR uploadtime = '" + yearmonth + "20' OR uploadtime = '" + yearmonth + "21'";
	}else if(day.equals("20")){
		week = "'" + yearmonth + "15' OR uploadtime = '" + yearmonth + "16' OR uploadtime = '" + yearmonth + "17' OR uploadtime = '" + yearmonth + "18' OR uploadtime = '" + yearmonth + "19' OR uploadtime = '" + yearmonth + "20' OR uploadtime = '" + yearmonth + "21'";
	}else if(day.equals("21")){
		week = "'" + yearmonth + "22' OR uploadtime = '" + yearmonth + "23' OR uploadtime = '" + yearmonth + "24' OR uploadtime = '" + yearmonth + "25' OR uploadtime = '" + yearmonth + "26' OR uploadtime = '" + yearmonth + "27' OR uploadtime = '" + yearmonth + "28' OR uploadtime = '" + yearmonth + "29' OR uploadtime = '" + yearmonth + "30' OR uploadtime = '" + yearmonth + "31'";
	}else if(day.equals("22")){
		week = "'" + yearmonth + "22' OR uploadtime = '" + yearmonth + "23' OR uploadtime = '" + yearmonth + "24' OR uploadtime = '" + yearmonth + "25' OR uploadtime = '" + yearmonth + "26' OR uploadtime = '" + yearmonth + "27' OR uploadtime = '" + yearmonth + "28' OR uploadtime = '" + yearmonth + "29' OR uploadtime = '" + yearmonth + "30' OR uploadtime = '" + yearmonth + "31'";
	}else if(day.equals("23")){
		week = "'" + yearmonth + "22' OR uploadtime = '" + yearmonth + "23' OR uploadtime = '" + yearmonth + "24' OR uploadtime = '" + yearmonth + "25' OR uploadtime = '" + yearmonth + "26' OR uploadtime = '" + yearmonth + "27' OR uploadtime = '" + yearmonth + "28' OR uploadtime = '" + yearmonth + "29' OR uploadtime = '" + yearmonth + "30' OR uploadtime = '" + yearmonth + "31'";
	}else if(day.equals("24")){
		week = "'" + yearmonth + "22' OR uploadtime = '" + yearmonth + "23' OR uploadtime = '" + yearmonth + "24' OR uploadtime = '" + yearmonth + "25' OR uploadtime = '" + yearmonth + "26' OR uploadtime = '" + yearmonth + "27' OR uploadtime = '" + yearmonth + "28' OR uploadtime = '" + yearmonth + "29' OR uploadtime = '" + yearmonth + "30' OR uploadtime = '" + yearmonth + "31'";
	}else if(day.equals("25")){
		week = "'" + yearmonth + "22' OR uploadtime = '" + yearmonth + "23' OR uploadtime = '" + yearmonth + "24' OR uploadtime = '" + yearmonth + "25' OR uploadtime = '" + yearmonth + "26' OR uploadtime = '" + yearmonth + "27' OR uploadtime = '" + yearmonth + "28' OR uploadtime = '" + yearmonth + "29' OR uploadtime = '" + yearmonth + "30' OR uploadtime = '" + yearmonth + "31'";
	}else if(day.equals("26")){
		week = "'" + yearmonth + "22' OR uploadtime = '" + yearmonth + "23' OR uploadtime = '" + yearmonth + "24' OR uploadtime = '" + yearmonth + "25' OR uploadtime = '" + yearmonth + "26' OR uploadtime = '" + yearmonth + "27' OR uploadtime = '" + yearmonth + "28' OR uploadtime = '" + yearmonth + "29' OR uploadtime = '" + yearmonth + "30' OR uploadtime = '" + yearmonth + "31'";
	}else if(day.equals("27")){
		week = "'" + yearmonth + "22' OR uploadtime = '" + yearmonth + "23' OR uploadtime = '" + yearmonth + "24' OR uploadtime = '" + yearmonth + "25' OR uploadtime = '" + yearmonth + "26' OR uploadtime = '" + yearmonth + "27' OR uploadtime = '" + yearmonth + "28' OR uploadtime = '" + yearmonth + "29' OR uploadtime = '" + yearmonth + "30' OR uploadtime = '" + yearmonth + "31'";
	}else if(day.equals("28")){
		week = "'" + yearmonth + "22' OR uploadtime = '" + yearmonth + "23' OR uploadtime = '" + yearmonth + "24' OR uploadtime = '" + yearmonth + "25' OR uploadtime = '" + yearmonth + "26' OR uploadtime = '" + yearmonth + "27' OR uploadtime = '" + yearmonth + "28' OR uploadtime = '" + yearmonth + "29' OR uploadtime = '" + yearmonth + "30' OR uploadtime = '" + yearmonth + "31'";
	}else if(day.equals("29")){
		week = "'" + yearmonth + "22' OR uploadtime = '" + yearmonth + "23' OR uploadtime = '" + yearmonth + "24' OR uploadtime = '" + yearmonth + "25' OR uploadtime = '" + yearmonth + "26' OR uploadtime = '" + yearmonth + "27' OR uploadtime = '" + yearmonth + "28' OR uploadtime = '" + yearmonth + "29' OR uploadtime = '" + yearmonth + "30' OR uploadtime = '" + yearmonth + "31'";
	}else if(day.equals("30")){
		week = "'" + yearmonth + "22' OR uploadtime = '" + yearmonth + "23' OR uploadtime = '" + yearmonth + "24' OR uploadtime = '" + yearmonth + "25' OR uploadtime = '" + yearmonth + "26' OR uploadtime = '" + yearmonth + "27' OR uploadtime = '" + yearmonth + "28' OR uploadtime = '" + yearmonth + "29' OR uploadtime = '" + yearmonth + "30' OR uploadtime = '" + yearmonth + "31'";
	}else if(day.equals("31")){
		week = "'" + yearmonth + "22' OR uploadtime = '" + yearmonth + "23' OR uploadtime = '" + yearmonth + "24' OR uploadtime = '" + yearmonth + "25' OR uploadtime = '" + yearmonth + "26' OR uploadtime = '" + yearmonth + "27' OR uploadtime = '" + yearmonth + "28' OR uploadtime = '" + yearmonth + "29' OR uploadtime = '" + yearmonth + "30' OR uploadtime = '" + yearmonth + "31'";
	}

	sql = "SELECT r1.img_id,r1.total_good,r1.img_name,(SELECT count(r2.total_good) FROM imagetable as r2 ) + 1 as rank FROM imagetable as r1 WHERE  r1.img_delete=true AND (uploadtime = "+ week + " )ORDER BY r1.total_good DESC LIMIT 3 ;";
	st = con.prepareStatement(sql);
	ResultSet rs3 = st.executeQuery();

	//データベースから値を引き出すためのwhile文
	while(rs3.next()){
		arrayId3.add(rs3.getInt("img_id"));
		arrayName3.add(rs3.getString("img_name"));
		arrayGood3.add(rs3.getInt("total_good"));
	}

	//総合、デイ、ウィークそれぞれに存在する写真が2以下の場合に対する処理
	if(arrayName.size() == 2){
		arrayName.add("Noimage.jpg");
		arrayGood.add(0);
	}
	if(arrayName.size() == 1){
		arrayName.add("Noimage.jpg");
		arrayName.add("Noimage.jpg");
		arrayGood.add(0);
		arrayGood.add(0);
	}
	if(arrayName.size() == 0){
		arrayName.add("Noimage.jpg");
		arrayName.add("Noimage.jpg");
		arrayName.add("Noimage.jpg");
		arrayGood.add(0);
		arrayGood.add(0);
		arrayGood.add(0);
	}
	if(arrayName2.size() == 2){
		arrayName2.add("Noimage.jpg");
		arrayGood2.add(0);
	}
	if(arrayName2.size() == 1){
		arrayName2.add("Noimage.jpg");
		arrayName2.add("Noimage.jpg");
		arrayGood2.add(0);
		arrayGood2.add(0);
	}
	if(arrayName2.size() == 0){
		arrayName2.add("Noimage.jpg");
		arrayName2.add("Noimage.jpg");
		arrayName2.add("Noimage.jpg");
		arrayGood2.add(0);
		arrayGood2.add(0);
		arrayGood2.add(0);
	}
	if(arrayName3.size() == 2){
		arrayName3.add("Noimage.jpg");
		arrayGood3.add(0);
	}
	if(arrayName3.size() == 1){
		arrayName3.add("Noimage.jpg");
		arrayName3.add("Noimage.jpg");
		arrayGood3.add(0);
		arrayGood3.add(0);
	}
	if(arrayName3.size() == 0){
		arrayName3.add("Noimage.jpg");
		arrayName3.add("Noimage.jpg");
		arrayName3.add("Noimage.jpg");
		arrayGood3.add(0);
		arrayGood3.add(0);
		arrayGood3.add(0);
	}
	//接続を切断
	DbUtil.disconnect(con);
	DbUtil.disstmt(st);
 %>


	<h3>いいなぁランキング</h3>
	<div class="posi-right">(0時更新)</div>
	<h4>総合</h4>
	<table class="rank">
		<% if(!arrayName.get(0).equals("Noimage.jpg")){ %>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/oukan1.png" class="oukan"></td>
			<td class="posi-center"><a href='photo.jsp?img_id=<%=arrayId.get(0)%>'><img src="contents/<%=arrayName.get(0)%>" class="ranking" ></a><div class="ranking-comment">いいなぁ(<%=arrayGood.get(0) %>)</div></td>
		</tr>
		<%
		}
		else{
		%>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/oukan1.png" class="oukan"></td>
			<td class="posi-center"><img src="contents/<%=arrayName.get(0)%>" class="ranking" ><div class="ranking-comment">いいなぁ(<%=arrayGood.get(0) %>)</div></td>
		</tr>
		<% } %>
		<% if(!arrayName.get(1).equals("Noimage.jpg")){ %>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2548.png" class="oukan"></td>
			<td class="posi-center"><a href='photo.jsp?img_id=<%=arrayId.get(1)%>'><img src="contents/<%=arrayName.get(1)%>" class="ranking" ></a><div class="ranking-comment">いいなぁ(<%=arrayGood.get(1) %>)</div></td>
		</tr>
		<%
		}
		else{
		%>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2548.png" class="oukan"></td>
			<td class="posi-center"><img src="contents/<%=arrayName.get(1)%>" class="ranking" ><div class="ranking-comment">いいなぁ(<%=arrayGood.get(1) %>)</div></td>
		</tr>
		<% } %>
				<% if(!arrayName.get(2).equals("Noimage.jpg")){ %>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2547.png" class="oukan"></td>
			<td class="posi-center"><a href='photo.jsp?img_id=<%=arrayId.get(2)%>'><img src="contents/<%=arrayName.get(2)%>" class="ranking" ></a><div class="ranking-comment">いいなぁ(<%=arrayGood.get(2) %>)</div></td>
		</tr>
		<%
		}
		else{
		%>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2547.png" class="oukan"></td>
			<td class="posi-center"><img src="contents/<%=arrayName.get(2)%>" class="ranking" ><div class="ranking-comment">いいなぁ(<%=arrayGood.get(2) %>)</div></td>
		</tr>
		<% } %>
	</table><hr>

	<h4 id="daily">デイリー</h4>
	<table class="rank">
		<% if(!arrayName2.get(0).equals("Noimage.jpg")){ %>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/oukan1.png" class="oukan"></td>
			<td class="posi-center"><a href='photo.jsp?img_id=<%=arrayId2.get(0)%>'><img src="contents/<%=arrayName2.get(0)%>" class="ranking" ></a><div class="ranking-comment">いいなぁ(<%=arrayGood2.get(0) %>)</div></td>
		</tr>
		<%
		}
		else{
		%>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/oukan1.png" class="oukan"></td>
			<td class="posi-center"><img src="contents/<%=arrayName2.get(0)%>" class="ranking" ><div class="ranking-comment">いいなぁ(<%=arrayGood2.get(0) %>)</div></td>
		</tr>
		<% } %>
		<% if(!arrayName2.get(1).equals("Noimage.jpg")){ %>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2548.png" class="oukan"></td>
			<td class="posi-center"><a href='photo.jsp?img_id=<%=arrayId2.get(1)%>'><img src="contents/<%=arrayName2.get(1)%>" class="ranking" ></a><div class="ranking-comment">いいなぁ(<%=arrayGood2.get(1) %>)</div></td>
		</tr>
		<%
		}
		else{
		%>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2548.png" class="oukan"></td>
			<td class="posi-center"><img src="contents/<%=arrayName2.get(1)%>" class="ranking" ><div class="ranking-comment">いいなぁ(<%=arrayGood2.get(1) %>)</div></td>
		</tr>
		<% } %>
		<% if(!arrayName2.get(2).equals("Noimage.jpg")){ %>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2547.png" class="oukan"></td>
			<td class="posi-center"><a href='photo.jsp?img_id=<%=arrayId2.get(2)%>'><img src="contents/<%=arrayName2.get(2)%>" class="ranking" ></a><div class="ranking-comment">いいなぁ(<%=arrayGood2.get(2) %>)</div></td>
		</tr>
		<%
		}
		else{
		%>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2547.png" class="oukan"></td>
			<td class="posi-center"><img src="contents/<%=arrayName2.get(2)%>" class="ranking" ><div class="ranking-comment">いいなぁ(<%=arrayGood2.get(2) %>)</div></td>
		</tr>
		<% } %>
	</table><hr>

	<h4>ウィークリー</h4>
	<table class="rank">
		<% if(!arrayName3.get(0).equals("Noimage.jpg")){ %>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/oukan1.png" class="oukan"></td>
			<td class="posi-center"><a href='photo.jsp?img_id=<%=arrayId3.get(0)%>'><img src="contents/<%=arrayName3.get(0)%>" class="ranking" ></a><div class="ranking-comment">いいなぁ(<%=arrayGood3.get(0) %>)</div></td>
		</tr>
		<%
		}
		else{
		%>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/oukan1.png" class="oukan"></td>
			<td class="posi-center"><img src="contents/<%=arrayName3.get(0)%>" class="ranking" ><div class="ranking-comment">いいなぁ(<%=arrayGood3.get(0) %>)</div></td>
		</tr>
		<% } %>
		<% if(!arrayName3.get(1).equals("Noimage.jpg")){ %>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2548.png" class="oukan"></td>
			<td class="posi-center"><a href='photo.jsp?img_id=<%=arrayId3.get(1)%>'><img src="contents/<%=arrayName3.get(1)%>" class="ranking" ></a><div class="ranking-comment">いいなぁ(<%=arrayGood3.get(1) %>)</div></td>
		</tr>
		<%
		}
		else{
		%>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2548.png" class="oukan"></td>
			<td class="posi-center"><img src="contents/<%=arrayName3.get(1)%>" class="ranking" ><div class="ranking-comment">いいなぁ(<%=arrayGood3.get(1) %>)</div></td>
		</tr>
		<% } %>
		<% if(!arrayName3.get(2).equals("Noimage.jpg")){ %>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2547.png" class="oukan"></td>
			<td class="posi-center"><a href='photo.jsp?img_id=<%=arrayId3.get(2)%>'><img src="contents/<%=arrayName3.get(2)%>" class="ranking" ></a><div class="ranking-comment">いいなぁ(<%=arrayGood3.get(2) %>)</div></td>
		</tr>
		<%
		}
		else{
		%>
		<tr>
			<td class="ver-center" class="50px"><img src="img/material/pics2547.png" class="oukan"></td>
			<td class="posi-center"><img src="contents/<%=arrayName3.get(2)%>" class="ranking" ><div class="ranking-comment">いいなぁ(<%=arrayGood3.get(2) %>)</div></td>
		</tr>
		<% } %>
	</table>

</body>
</html:html>


