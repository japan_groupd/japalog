<%@page import="jp.co.phopan.db.DbDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="java.sql.* " import="jp.co.phopan.db.DbUtil" import="jp.co.phopan.db.*"
    %>
    <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%UserTable user = (UserTable)session.getAttribute("loginUser"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/calendar.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<link type="text/css" href="css/base.css" rel="stylesheet">
<title>phopan</title>
</head>
<body>
<%
	String sql="SELECT * FROM imagetable where user_id=? ORDER BY img_id DESC";

	//データベース接続
	Connection con =DbUtil.connect();

	PreparedStatement stmt = con.prepareStatement(sql);

	//セッションからuserid
	//現状nullpointエラーが出る-----------------------------------------------------
	stmt.setInt(1, user.getUser_id());

	ResultSet rs = stmt.executeQuery();

	ResultSet urs = DbDao.select_serchUserid(con,user.getUser_id());

%>
	<div id="basesize">
			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
			<jsp:include page="/search.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
			<!--  ▼ワッパー -->
			<div id="wrapper">
				<div id="sidebar-r">
					<h3>マイページメニュー</h3>
					<table>
						<tr>
							<td><h2><a href="photoinsert.jsp">投稿する</a></h2></td>
						</tr>
						<tr>
							<td><h2><a href="profilechange.jsp">ユーザ情報変更</a></h2></td>
						</tr>
					</table>
				</div>
				<!--  ▽メイン -->
				<div id="main">
					<h3><%=user.getUser_name() %>さんのマイページ</h3>
					<h2>写真一覧</h2>
					<%
					while(rs.next()){
					if(rs.getBoolean("img_delete")){
					%>
					<table  class="photo">
						<tr><td class="info"><a href='photo.jsp?img_id=<%=rs.getInt("img_id")%>'><img src="<%="contents/"+rs.getString("img_name") %>" class="thumbnail"></a></td></tr>
						<tr><td class="info"><h6><%out.println(rs.getString("title"));%></h6></td></tr>
						<tr><td class="info">投稿日 <%out.println(rs.getDate("uploadtime"));%><br>
								投稿者 <%=user.getUser_name()%><br>
								カテゴリ <%out.print(DbDao.selectCategory(rs.getInt("category_id")));%><br>
								いいなぁ(<%out.println(rs.getInt("total_good"));%>)
							</td>
						</tr>

						<tr>
						<td>

						<form action='updatephoto.jsp' method='post'>
						<input type=hidden  name="img_id" value="<%=rs.getString("img_id")%>">
						<input type='submit' class='button' value='更新'></form>

						<form action='deletephoto.jsp' method='post'>
						<input type=hidden  name="img_id" value="<%=rs.getString("img_id")%>">
						<input type='submit' class='button' value='削除'></form></td>
						</tr>
					</table>

					<%}
					}
					%>
				</div>
				<%
					//接続を切断
					stmt.close();
					DbUtil.disconnect(con);
				%>

				<!--  △メイン -->
			</div>
			<!--  ▲ワッパー -->
			<!--  ▼フッター -->
			<div id="footer">
				<a href="#">ページ上部に戻る</a>
			</div>
			<!--  ▲フッター -->
		</div>
</body>
</html:html>
