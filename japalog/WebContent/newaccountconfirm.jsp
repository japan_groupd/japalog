<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>phopan</title>
	<script type="text/javascript" src="js/main.js"></script>
	<link type="text/css" href="css/base.css" rel="stylesheet">
</head>
<body>
<div id="basesize">
	<!--  ▼ヘッダー -->
	<jsp:include page="/header.jsp" flush="true" />
	<!--  ▲ヘッダー -->


	<!--  ▼ワッパー -->
	<div id="wrapper">
		<!--  ▽メニュー -->

		<!--  △メニュー -->

		<!--  ▽メイン -->
		<div id="main" class="posi-center">
			<html:form action="/newAccountConfirm"  method="POST" enctype="multipart/form-data">
			<table class="edit posi-center">
				<tr>
					<td colspan="2"><h2>新規ユーザ登録</h2></td>
				</tr>
				<tr>
					<td colspan="2"><h6>本当にこれでよろしいですか？</h6></td>
				</tr>
				<tr>
					<td class="posi-left" colspan="2"><font color ="red">※</font>ユーザ名</td>
				</tr>
				<tr>
					<td colspan="2"><html:text property="user_name" disabled="true"/><br></td>
					<html:hidden property="user_name"></html:hidden>
				</tr>
				<tr>
					<td class="posi-left" colspan="2"><font color ="red">※</font>パスワード</td>
				</tr>
				<tr>
					<td colspan="2"><html:password property="pass"   disabled="true"/><br></td>
					<html:hidden property="pass"></html:hidden>
					<html:hidden property="rekeypass"></html:hidden>
				</tr>
				<tr>
					<td><html:submit property="submit" styleClass="button" value="登録" /></html:form></td>

					<td><html:form action="newAccount.jsp"><html:submit property="submit" styleClass="button" value="戻る" /></html:form></td>
				</tr>
			</table>

		</div>
		<!--  △メイン -->
	</div>
	<!--  ▲ワッパー -->


	<!--  ▼フッター -->
	<div id="footer">
		<a href="#">ページ上部に戻る</a>
	</div>
	<!--  ▲フッター -->

</div>
</body>
</html:html>