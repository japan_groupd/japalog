<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="jp.co.phopan.db.DbUtil" import="jp.co.phopan.db.*" import="java.sql.*"
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<% String error = (String)request.getAttribute("error"); %>
<% String titleerror = (String)request.getAttribute("titleerror"); %>
<% String deterror = (String)request.getAttribute("deterror"); %>

<html:html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>phopan</title>
		<script type="text/javascript" src="js/calendar.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<link type="text/css" href="css/base.css" rel="stylesheet">
	</head>
	<script language="JavaScript"><!--
	function setData(param1,param2)
	{
		var start = document.photoForm.introduction.selectionStart;	//選択開始位置を取得
		var end = document.photoForm.introduction.selectionEnd;	//選択最後位置を取得
		var str = document.photoForm.introduction.value.slice(start,end);	//選択開始位置から選択最後位置までの文字列を取得
		document.photoForm.introduction.value = document.photoForm.introduction.value.substr(0,start) + param1 + str + param2 + document.photoForm.introduction.value.substr(end,document.photoForm.introduction.value.length);
	}

	// --></script>
	<body>
		<%

		int imgid = Integer.parseInt(request.getParameter("img_id"));

		String sql="SELECT * FROM imagetable where img_id=? ";

		//データベース接続
		Connection con =DbUtil.connect();

		PreparedStatement stmt = con.prepareStatement(sql);

		stmt.setInt(1,imgid);

		ResultSet rs = stmt.executeQuery();

		while(rs.next()){

		%>

		<div id="basesize">
			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
			<jsp:include page="/search.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
			<!--  ▼ワッパー -->
			<div id="wrapper">
			<!--  ▼メイン -->
				<div id="main">
					<h2  class="posi-center">投稿内容更新</h2>
					<h6 class="posi-center">この写真の投稿内容を更新してください</h6>

					<font color="red">
							<% if(error!=null){%><%=error %><br><% } %>
							<% if(titleerror!=null){%><%=titleerror %><br><% } %>
							<% if(deterror!=null){%><%=deterror %><br><% } %>
					</font>


					<html:form action="/updatePhoto" enctype="multipart/form-data" >
					<table  class="posi-center" >
						<tr>
							<td colspan="3"><h6>投稿写真</h6></td>
						</tr>
						<tr>
							<td colspan="3"><img src="contents/<%=rs.getString("img_name") %>"/></td>
						</tr>
						<tr>
							<td colspan="3"><h6>カテゴリ</h6></td>
						</tr>
						<tr>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="1"
								<% if(rs.getInt("category_id")==1) {%> checked <%} %>
								>風景・自然</p>
							</td>
							<td>
								<p ><input class="radio" type="radio" name="category_id" value="2"
								<% if(rs.getInt("category_id")==2) {%> checked <%} %>
								>人物</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="3"
								<% if(rs.getInt("category_id")==3) {%> checked <%} %>
								>乗り物</p>
							</td>
						</tr>
						<tr>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="4"
								<% if(rs.getInt("category_id")==4) {%> checked <%} %>
								>動物</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="5"
								<% if(rs.getInt("category_id")==5) {%> checked <%} %>
								>イラストアート</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="6"
								<% if(rs.getInt("category_id")==6) {%> checked <%} %>
								>花・植物</p>
							</td>
						</tr>
						<tr>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="7"
								<% if(rs.getInt("category_id")==7) {%> checked <%} %>
								>建物・町並み</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="8"
								<% if(rs.getInt("category_id")==8) {%> checked <%} %>
								>グルメ</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="9"
								<% if(rs.getInt("category_id")==9) {%> checked <%} %>
								>イベント</p>
							</td>
						</tr>
						<tr>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="10"
								<% if(rs.getInt("category_id")==10) {%> checked <%} %>
								>インテリア</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="11"
								<% if(rs.getInt("category_id")==11) {%> checked <%} %>
								>スポーツ</p>
							</td>
							<td>
								<p><input class="radio" type="radio" name="category_id" value="12"
								<% if(rs.getInt("category_id")==12) {%> checked <%} %>
								>その他</p>
							</td>
						</tr>
						<tr>
							<td colspan="3"><h6>写真タイトル(1～13文字)</h6></td>
						</tr>
						<tr>
							<td colspan="3"><input type="text" name="title" value="<%=rs.getString("title")%>"></td>
						</tr>
						<tr>
							<td colspan="3"><h6>詳細(1～200文字)</h6>
								<input type="button" class="button" value="太字" onClick="setData('<b>','</b>')">
								<input type="button" class="button" value="斜体" onClick="setData('<i>','</i>')">
								<input type="button" class="button" value="打ち消し" onClick="setData('<s>','</s>')">
								<input type="button" class="button" value="赤色" onClick="setData('<font color=&quot#ff0000&quot>','</font>')">
								<input type="button" class="button" value="緑色" onClick="setData('<font color=&quot#00ff00&quot>','</font>')">
								<input type="button" class="button" value="青色" onClick="setData('<font color=&quot#0000ff&quot>','</font>')"><BR>
								<textarea name="introduction" class="ins"><%=rs.getString("details")%></textarea></td>
						</tr>
					</table>
					<input type="hidden" name="img_id" value="<%=rs.getString("img_id")%>">
					<input type="hidden" name="img_name" value="<%=rs.getString("img_name")%>">

					<table class="posi-center"><tr><td>
					<input type="submit" class="button" value="確認">
					<!--  style="position: absolute; left: 40%;" //めも -->
					<input type="button" class="button" onClick="document.location='mypage.jsp';" value="戻る">
					</td></tr></table>
				</html:form>
				</div>
			</div>
			<!--  ▲ワッパー -->
			<!--  ▼フッター -->
			<div id="footer">
				<a href="#basesize">ページ上部に戻る</a>
			</div>
			<!--  ▲フッター -->
		</div>
		<%
				}
					//接続を切断
					stmt.close();
					DbUtil.disconnect(con);
				%>
	</body>
</html:html>
