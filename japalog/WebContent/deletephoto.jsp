<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="jp.co.phopan.db.DbUtil" import="jp.co.phopan.db.*" import="java.sql.*"
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>


<html:html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>phopan</title>
		<script type="text/javascript" src="js/calendar.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<link type="text/css" href="css/base.css" rel="stylesheet">
	</head>
	<script language="JavaScript"><!--
	function setData(param1,param2)
	{
		var start = document.photoForm.introduction.selectionStart;	//選択開始位置を取得
		var end = document.photoForm.introduction.selectionEnd;	//選択最後位置を取得
		var str = document.photoForm.introduction.value.slice(start,end);	//選択開始位置から選択最後位置までの文字列を取得
		document.photoForm.introduction.value = document.photoForm.introduction.value.substr(0,start) + param1 + str + param2 + document.photoForm.introduction.value.substr(end,document.photoForm.introduction.value.length);
	}

	function setLink(param1,param2,param3)
	{
		var start = document.photoForm.introduction.selectionStart;	//選択開始位置を取得
		var end = document.photoForm.introduction.selectionEnd;	//選択最後位置を取得
		var str = document.photoForm.introduction.value.slice(start,end);	//選択開始位置から選択最後位置までの文字列を取得
		document.photoForm.introduction.value = document.photoForm.introduction.value.substr(0,start) + param1 + str + param2 + str + param3 + document.photoForm.introduction.value.substr(end,document.photoForm.introduction.value.length);
	}
	// --></script>
	<body>
		<%

		int imgid = Integer.parseInt(request.getParameter("img_id"));

		String sql="SELECT * FROM imagetable where img_id=? ";

		//データベース接続
		Connection con =DbUtil.connect();

		PreparedStatement stmt = con.prepareStatement(sql);

		stmt.setInt(1,imgid);

		ResultSet rs = stmt.executeQuery();

		while(rs.next()){
			String category_name = DbDao.selectCategory(rs.getInt("category_id"));

		%>

		<div id="basesize">
			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/headeradmin.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
			<!--  ▼ワッパー -->
			<div id="wrapper">
			<!--  ▼メイン -->
				<div id="main" class="column">
					<h2  class="posi-center">写真削除</h2>
					<h6 class="posi-center">この写真を削除しますか？</h6>
					<html:form action="/deletePhoto" enctype="multipart/form-data">
					<table  class="posi-center" >
						<tr>
							<td colspan="3"><h6>投稿写真</h6></td>
						</tr>
						<tr>
							<td colspan="3"><img src="contents/<%=rs.getString("img_name") %>"/></td>
						</tr>
						<tr>
							<td colspan="3"><h6>カテゴリ</h6></td>
						</tr>
						<tr>
							<td colspan="3"><input type="text" name="category_name" value="<%=category_name%>" disabled="true"></td>
						</tr>
						<tr>
							<td colspan="3"><h6>写真タイトル(1～13文字)</h6></td>
						</tr>
						<tr>
							<td colspan="3"><input type="text" name="title" value="<%=rs.getString("title")%>" disabled="true"></td>
						</tr>
						<tr>
							<td colspan="3"><h6>詳細(1～200文字)</h6>
								<textarea name="introduction" class="ins" disabled="true"><%=rs.getString("details")%></textarea></td>
						</tr>
					</table>
					<input type="hidden" name="img_id" value="<%=rs.getString("img_id")%>">

					<table class="posi-center"><tr><td>
					<input type="submit" class="button" value="削除">
					<!--  style="position: absolute; left: 40%;" //めも -->
					<input type="button" class="button" onClick="document.location='mypage.jsp';" value="戻る">
					</td></tr></table>
				</html:form>
				</div>
			</div>
			<!--  ▲ワッパー -->
			<!--  ▼フッター -->
			<div id="footer">
				<a href="#basesize">ページ上部に戻る</a>
			</div>
			<!--  ▲フッター -->
		</div>
		<%
				}
					//接続を切断
					stmt.close();
					DbUtil.disconnect(con);
				%>
	</body>
</html:html>
