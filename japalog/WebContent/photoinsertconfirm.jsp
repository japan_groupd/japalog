<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%String filename =(String)request.getAttribute("fileName");%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
		<title>phopan</title>
		<script type="text/javascript" src="js/calendar.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<link type="text/css" href="css/base.css" rel="stylesheet">
	</head>
	<body>
		<div id="basesize">
		<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
			<jsp:include page="/search.jsp" flush="true" />
		<!--  ▲ヘッダー＆カテゴリ -->
		<!--  ▼ワッパー -->
		<div id="wrapper">
			<div id="main" class="column">
				<h2  class="posi-center">写真投稿</h2>
				<h6 class="posi-center">これらの内容で追加しますか？</h6>
				<html:form action = "/insertPhotoConfirm" method="POST" enctype="multipart/form-data">
					<table class="posi-center">
						<tr>
							<td  colspan="3"><h6>投稿写真</h6></td>
						</tr>
						<tr>
							<td colspan="3"><html:img src="<%=filename%>"/>
							<html:hidden property="img_name"></html:hidden>
						</tr>
						<tr>
							<td colspan="3"><h6>カテゴリ</h6></td>
						</tr>
						<tr>
							<td colspan="3"><html:text property="category_name"  disabled="true"></html:text></td>
							<html:hidden property="category_id"></html:hidden>
						</tr>

						<tr>
							<td colspan="3"><h6>写真タイトル(1～13文字)</h6></td>
						</tr>
						<tr>
							<td colspan="3"><html:text property="title"  disabled="true"></html:text></td>
							<html:hidden property="title"></html:hidden>
						</tr>
						<tr>
							<td colspan="3"><h6>詳細(1～200文字)</h6></td>
						</tr>
						<tr>
							<td colspan="3"><html:textarea property="details" disabled="true"></html:textarea></td>
							<html:hidden property="details"></html:hidden>
						</tr>
					</table>
					<table class="posi-center"><tr><td>
						<tr>
							<td colspan="3">
								<html:submit property="submit" styleClass="button" value="投稿"></html:submit>
								<input type="hidden" name="img_id" value="<%=session.getAttribute("img_id")%>">
								<input type="button" class="button" onClick="document.location='photoinsert.jsp';" value="戻る">
							</td>
						</tr>
					</table>
					</html:form>
				</div>
			</div>
			<!--  â²ã¯ããã¼ -->
			<!--  â¼ããã¿ã¼ -->
			<div id="footer">
				<a href="#basesize">ページ上部に戻る</a>
			</div>
			<!--  â²ããã¿ã¼ -->
		</div>
	</body>
</html>