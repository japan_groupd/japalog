<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<% String report = (String)request.getAttribute("report"); %>
<% int imgid = (Integer)request.getAttribute("imgid"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="css/base.css" rel="stylesheet">
<title>phopan</title>
</head>
<body>
<h3>通報理由</h3>

		選択した画像で通報理由に該当する項目はこちらでよろしいでしょうか。

		<br>
		<br>

		<div class="posi-center">
			<% if(report.equals("copyright")){%>
			<%="著作権で問題が生じる投稿" %>
			<% }else if(report.equals("portrait")){%>
			<%="肖像権で問題が生じる投稿"%>
        	<% }else if(report.equals("infringement")){%>
        	<%="特定の個人に対する人権侵害、中傷、悪口で問題が生じる投稿"%>
      		<% }else if(report.equals("sexual")){%>
        	<%="差別的な表現、性的な表現、暴力的な表現などで問題が生じる投稿"%>
        	<% }%>
		</div>

		<br>
		<br>

		<table class="posi-center">
			<tr>
				<td>
					<html:form action="reportPushConfirm">
						<input class="button" type="submit" value="報告">
 						<input type="hidden" name = "report" value =<%=report %>>
						<input type="hidden" name = "imgid" value =<%=imgid %>>
					</html:form>
				</td>
				<td>
					<form action="reportpush.jsp">
						<input type="submit" value="戻る"  class="button">
					</form>
				</td>
			</tr>
		</table>
</body>
</html>