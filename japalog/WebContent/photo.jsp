<%@ page language="java" import="java.util.*" import="jp.co.phopan.db.ContentsTable" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ page import = "java.sql.Connection" %>
<%@ page import = "jp.co.phopan.db.DbUtil" %>
<%@ page import = "jp.co.phopan.db.DbDao" %>
<%@ page import = "java.sql.PreparedStatement" %>
<%@ page import = "java.sql.ResultSet" %>
<%@ page import = "java.sql.Connection" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "java.text.SimpleDateFormat" %>
<%@ page import = "java.text.DateFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/main.js"></script>
<link type="text/css" href="css/base.css" rel="stylesheet">
<title>phopan</title>
</head>
	<body>
	<%

	int imgid = Integer.parseInt(request.getParameter("img_id"));
	/*int imgid = 9;*/
	Connection con = DbUtil.connect();
	String sql = "SELECT * FROM imagetable WHERE img_id = " + imgid + ";";
	PreparedStatement st = con.prepareStatement(sql);
	ResultSet rs = st.executeQuery();
	ArrayList<Object> array = new ArrayList<Object>();
	int accesscount = 0;//アクセスの数を入力する為の変数

	//データベースから値を引き出すためのwhile文
	while(rs.next()){
		array.add(rs.getInt("img_id"));      //array.get(0)はimg_id
		array.add(rs.getInt("user_id"));	 //array.get(1)はuser_id
		array.add(rs.getString("img_name")); //array.get(2)はimg_name
		array.add(rs.getInt("category_id")); //array.get(3)はcategory_id
		array.add(rs.getString("title"));    //array.get(4)はtitle
		array.add(rs.getDate("uploadtime")); //array.get(5)はuploadtime
		array.add(rs.getString("details"));  //array.get(6)はdetails
		array.add(rs.getInt("total_good"));  //array.get(7)はtotal_good
		accesscount = rs.getInt("access_count")+1;
	}

	sql = "SELECT user_name FROM usertable WHERE user_id = " + array.get(1) + ";";
	st = con.prepareStatement(sql);
	ResultSet rs2 = st.executeQuery();
	String username = "";
	while(rs2.next()){
		username = rs2.getString("user_name");
	}

	sql = "SELECT category_name FROM categorytable WHERE category_id = " + array.get(3) + ";";
	st = con.prepareStatement(sql);
	ResultSet rs3 = st.executeQuery();
	String categoryname = "";
	while(rs3.next()){
		categoryname = rs3.getString("category_name");
	}

	sql = "UPDATE imagetable SET access_count = " + accesscount +  " WHERE img_id = " + imgid + ";";
	st = con.prepareStatement(sql);
	int rsUpdate = st.executeUpdate();

	//接続を切断
	DbUtil.disconnect(con);
	DbUtil.disstmt(st);
	%>
	<script>function koshin(){location.reload();}</script>
		<div id="basesize">
			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
			<jsp:include page="/search.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
			<!--  ▼ワッパー -->
			<div id="wrapper">
				<!--  ▽右側表示 -->
				<div id="sidebar-r">
				<jsp:include page="/ranking.jsp" flush="true" />
				</div>
				<!--  △右側表示 -->
				<!--  ▽メイン -->
				<div id="main">
					<h3><%=array.get(4) %></h3>
					<table  class="posi-center">
						<tr>
							<td rowspan="7"><a href="photodl.jsp?img_name=contents/<%=array.get(2) %>" target="_blank"><img src="contents/<%=array.get(2)%>" class="min-max"  ></a></td>
							<td>投稿者：<%=username%></td>
						</tr>
						<tr>
							<td>投稿日時：<%=array.get(5) %></td>
						</tr>
						<tr>
							<td>カテゴリ：<%=categoryname %></td>
						</tr>
						<tr>
							<td>いいなぁ数：<%=array.get(7) %></td>
						</tr>
						<tr>
							<td>詳細：</td>
						</tr>
						<tr>
							<td><%=array.get(6) %></td>
						</tr>
						<tr>

							<html:form action="/goodPushAction">
							<input type="hidden" name="img_id" value="<%=imgid%>">
							<td><input type="submit" class="button" value="いいなぁ"  onClick="window.open('goodpush.jsp','','scrollbars,width=700,height=130,');koshin()"></td>
							</html:form>
						</tr>
					</table><br>



					<%

					//写真と投稿した人の他の写真を表示9--------------------------------------------

						String imgsql="SELECT * FROM imagetable where user_id=? ORDER BY img_id DESC";
						String useridsql="SELECT * FROM imagetable where img_id=?;";

							//データベース接続
						Connection imgcon =DbUtil.connect();


							//投稿した写真の投稿者idを取得

							PreparedStatement useridstmt = imgcon.prepareStatement(useridsql);


							useridstmt.setInt(1, imgid);

							ResultSet useridrs = useridstmt.executeQuery();

							PreparedStatement imgstmt = imgcon.prepareStatement(imgsql);

							int user_id;

							while(useridrs.next()){
							user_id=useridrs.getInt("user_id");
							imgstmt.setInt(1, user_id);
							}
							ResultSet imgrs = imgstmt.executeQuery();

							String user_name;
							ResultSet urs = DbDao.select_serchUserid(imgcon, 2);
							while(urs.next()){
								user_name=urs.getString("user_name");%>

						<h3><%=username%>さんの他の写真</h3>
						<div class="photolist">
							<%
							while(imgrs.next()){
							if(imgrs.getBoolean("img_delete")){
								if(imgid != imgrs.getInt("img_id")){


							%>
							<table class="photo">
								<tr>
									<td><%out.print(imgrs.getString("title")); %></td>
								</tr>
								<tr>
									<td><a href="photo.jsp?img_id=<%=imgrs.getInt("img_id")%>"><img src="contents/<%=imgrs.getString("img_name") %>" class="thumbnail"  ></a></td>
								</tr>
							</table>
						<%
								}
							}
							}%>
						</div>
						<%
						}
						//接続を切断
						useridstmt.close();
						imgstmt.close();
						DbUtil.disconnect(imgcon);
						%>

						<br clear="all"><br>


						<input type="button" class="button" value="通報" class="f-none" onClick="window.open('reportpush.jsp?img_id=<%=array.get(0) %>','','scrollbars,width=700,height=450,');">


					</div>
				</div>
				<!--  △メイン -->
			</div>
			<!--  ▲ワッパー -->
			<!--  ▼フッター -->
			<div id="footer">
				<a href="#">ページ上部に戻る</a>
			</div>
			<!--  ▲フッター -->
		</div>
	</body>
</html>