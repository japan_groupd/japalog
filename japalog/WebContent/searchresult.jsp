<%@ page language="java" import="java.util.*" import="jp.co.phopan.db.DbDao" import="jp.co.phopan.db.DbUtil" import="jp.co.phopan.db.JoinTable" import="jp.co.phopan.db.ConnectInfo" import="jp.co.phopan.db.JoinTable" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<% String catename = (String)request.getAttribute("catename"); %>
<% String stext = (String)request.getAttribute("stext"); %>
<% String k = (String)request.getAttribute("k"); %>
<% String count = (String)request.getAttribute("count"); %>
<% List<JoinTable> array = (ArrayList<JoinTable>)request.getAttribute("joinArray");%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>phopan</title>
<link href="css/base.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="basesize">
		<!--  ▼ヘッダー＆カテゴリ -->
		<jsp:include page="/header.jsp" flush="true"/>
		<jsp:include page="/search.jsp" flush="true"/>
 		<!--  ▲ヘッダー＆カテゴリ -->

		<!--  ▼ワッパー -->
		<div id="wrapper">
		<!--  ▽右側表示 -->
			<div id="sidebar-r">
				<jsp:include page="/ranking.jsp" flush="true" />
			</div>
			<!--  △右側表示 -->
	<!--  ▽メイン -->
			<div id="main">
			<h3><%=catename %> ＞ <%=stext %> ＞ <%if (k != null && k.length() != 0 && !k.equals("") ){out.print(k);}else{out.print("全件表示");} %> (<%=count%>件)</h3>
				<logic:iterate id="joinArray" name="joinArray" scope="request" indexId="idx">
				<table class="photo">
					<tr>
						<td class="info"><a href='photo.jsp?img_id=<%=array.get(idx).getImg_id()%>'><img src="contents/<%=array.get(idx).getImg_name()%>" class="thumbnail" /></a></td>
					</tr>
					<tr>
						<td class="info"><h6><bean:write name="joinArray" property="title" /></h6></td>
					</tr>
					<tr>
						<td class="info">投稿日:<bean:write name="joinArray" property="uploadtime" /><br>
							投稿者名:<bean:write name="joinArray" property="user_name" /><br>
							カテゴリ:<bean:write name="joinArray" property="category_name" /><br>
							いいなぁ(<bean:write name="joinArray" property="total_good" />)
						</td>
					</tr>
				</table>
				</logic:iterate>
			</div>
			<!--  △メイン -->
		</div>
		<!--  ▲ワッパー -->

		<!--  ▼フッター -->
		<div id="footer">
			<a href="#">ページ上部に戻る</a>
		</div>
		<!--  ▲フッター -->
	</div>
</body>
</html:html>