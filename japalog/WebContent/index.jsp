<!--
【index画面】＠作業中 06/30～
	担当：栗原
-->

<%@ page language="java" import="java.util.*" import="jp.co.phopan.db.JoinTable" import="jp.co.phopan.db.ConnectInfo" import="jp.co.phopan.db.DbDao" import="jp.co.phopan.db.DbUtil" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%

	ConnectInfo info = new ConnectInfo();						//DB接続情報
	List<JoinTable> joinAry = new ArrayList<JoinTable>();		//全テーブル要素を持ったリスト

	try {
		//=============================================================================================================
		info.setMy_con(DbUtil.connect());						//Connection接続
		info.setMy_stmt(DbUtil.stmt(info.getMy_con()));			//Statement接続


		//JOIN文を使用して全件検索
		info.setMy_Result(DbDao.selectNew(info.getMy_stmt()));
		while( (info.getMy_Result()).next() ){
			JoinTable objAl=new JoinTable();
			objAl.setImg_name(info.getMy_Result().getString("img_name"));
			objAl.setImg_id(info.getMy_Result().getInt("img_id"));
			objAl.setImg_delete(info.getMy_Result().getBoolean("img_delete"));
			objAl.setUser_name(info.getMy_Result().getString("user_name"));
			objAl.setUser_delete(info.getMy_Result().getBoolean("user_delete"));
			objAl.setCategory_name(info.getMy_Result().getString("category_name"));
			objAl.setTitle(info.getMy_Result().getString("title"));
			objAl.setUploadtime(info.getMy_Result().getString("uploadtime"));
			objAl.setTotal_good(info.getMy_Result().getInt("total_good"));
			objAl.setAccess_count(info.getMy_Result().getInt("access_count"));
			joinAry.add(objAl);
		}

		ArrayList<Integer> arraycategory = DbDao.selectCategoryCount();
		request.setAttribute("arraycategory", arraycategory);

		//=============================================================================================================
		DbUtil.disstmt(info.getMy_stmt());					//Statement切断
		DbUtil.disconnect(info.getMy_con());					//Connection切断
	} catch(Exception e) {
		e.printStackTrace();
	}
    request.setAttribute("joinArray", joinAry);

%>

<% List<JoinTable> array = (ArrayList<JoinTable>)request.getAttribute("joinArray");%>
<% ArrayList<Integer> arraycategory = (ArrayList<Integer>)request.getAttribute("arraycategory");%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>phopan</title>
<link type="text/css" href="css/base.css" rel="stylesheet">
</head>
<body>
		<div id="basesize">
			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
			<jsp:include page="/search.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
			<!--  ▼ワッパー -->
			<div id="wrapper">
				<!--  ▽右側表示 -->
				<div id="sidebar-r">
				<jsp:include page="/ranking.jsp" flush="true" />
				</div>
				<!--  △右側表示 -->
				<!--  ▽メイン -->
				<div id="main">
					<h3>最新投稿写真</h3>
				<logic:iterate id="joinArray" name="joinArray" scope="request" indexId="idx">
				<table class="photo">
					<tr>
						<td class="info"><a href='photo.jsp?img_id=<%=array.get(idx).getImg_id()%>'><img src="contents/<%=array.get(idx).getImg_name()%>" class="thumbnail" /></a></td>
					</tr>
					<tr>
						<td class="info"><h6><bean:write name="joinArray" property="title" /></h6></td>
					</tr>
					<tr>
						<td class="info">投稿日:<bean:write name="joinArray" property="uploadtime" /><br>
							投稿者名:<bean:write name="joinArray" property="user_name" /><br>
							カテゴリ:<bean:write name="joinArray" property="category_name" /><br>
							いいなぁ(<bean:write name="joinArray" property="total_good" />)
						</td>
					</tr>
				</table>
				</logic:iterate>

					<br clear="all"><br>









					<h3>カテゴリ一覧</h3>
					<table class="photo">
						<tr>
							<td class="info"><html:link href="search.do?sort=0&category_id=1" ><img src="img/landscape.jpg" class="cateimg"></html:link></td>
						</tr>
						<tr>
							<td class="info"><div align="center"><b>風景(<%=arraycategory.get(0)%>)</b></div></td>
						</tr>
						<tr>
							<td class="info"><br></td>
						</tr>
				</table>
				<table  class="photo">
					<tr>
						<td class="info"><html:link href="search.do?sort=0&category_id=2" ><img src="img/human.jpg" class="cateimg"></html:link></td>
					</tr>
					<tr>
						<td class="info"><div align="center"><b>人物(<%=arraycategory.get(1)%>)</b></div></td>
					</tr>
					<tr>
						<td class="info"><br></td>
					</tr>
				</table>
				<table  class="photo">
				<tr>
					<td class="info"><html:link href="search.do?sort=0&category_id=3" ><img src="img/vehicle.jpg" class="cateimg"></html:link></td>
				</tr>
				<tr>
						<td class="info"><div align="center"><b>乗り物(<%=arraycategory.get(2)%>)</b></div></td>
				</tr>
				<tr>
					<td class="info"><br></td>
				</tr>
				</table>
				<table  class="photo">
					<tr>
						<td class="info"><html:link href="search.do?sort=0&category_id=4" ><img src="img/animal.jpg" class="cateimg"></html:link></td>
					</tr>
					<tr>
						<td class="info"><div align="center"><b>動物(<%=arraycategory.get(3)%>)</b></div></td>
					</tr>
					<tr>
						<td class="info"><br></td>
					</tr>
				</table>
				<table  class="photo">
					<tr>
						<td class="info"><html:link href="search.do?sort=0&category_id=5" ><img src="img/illust.jpg" class="cateimg"></html:link></td>
					</tr>
					<tr>
					<td class="info"><div align="center"><b>イラスト・アート(<%=arraycategory.get(4)%>)</b></div></td>
				</tr>
				<tr>
						<td class="info"><br></td>
					</tr>
				</table>
				<table  class="photo">
					<tr>
						<td class="info"><html:link href="search.do?sort=0&category_id=6" ><img src="img/flower.jpg" class="cateimg"></html:link></td>
					</tr>
					<tr>
						<td class="info"><div align="center"><b>花・植物(<%=arraycategory.get(5)%>)</b></div></td>
					</tr>
					<tr>
						<td class="info"><br></td>
					</tr>
				</table>
				<table class="photo">
					<tr>
						<td class="info"><html:link href="search.do?sort=0&category_id=7" ><img src="img/building.jpg" class="cateimg"></html:link></td>
					</tr>
						<tr>
						<td class="info"><div align="center"><b>建物・街並み(<%=arraycategory.get(6)%>)</b></div></td>
					</tr>
					<tr>
						<td class="info"><br></td>
					</tr>
				</table>
				<table  class="photo">
					<tr>
						<td class="info"><html:link href="search.do?sort=0&category_id=8" ><img src="img/gourme.jpg" class="cateimg"></html:link></td>
					</tr>
					<tr>
						<td class="info"><div align="center"><b>グルメ(<%=arraycategory.get(7)%>)</b></div></td>
					</tr>
					<tr>
						<td class="info"><br></td>
					</tr>
				</table>
				<table  class="photo">
					<tr>
							<td class="info"><html:link href="search.do?sort=0&category_id=9" ><img src="img/event.jpg" class="cateimg"></html:link></td>
					</tr>
					<tr>
						<td class="info"><div align="center"><b>イベント(<%=arraycategory.get(8)%>)</b></div></td>
					</tr>
					<tr>
						<td class="info"><br></td>
					</tr>
				</table>
				<table  class="photo">
					<tr>
						<td class="info"><html:link href="search.do?sort=0&category_id=10" ><img src="img/interior.jpg" class="cateimg"></html:link></td>
					</tr>
					<tr>
						<td class="info"><div align="center"><b>インテリア・雑貨(<%=arraycategory.get(9)%>)</b></div></td>
					</tr>
					<tr>
						<td class="info"><br></td>
					</tr>
				</table>
				<table  class="photo">
					<tr>
						<td class="info"><html:link href="search.do?sort=0&category_id=11" ><img src="img/sports.jpg" class="cateimg"></html:link></td>
					</tr>
					<tr>
						<td class="info"><div align="center"><b>スポーツ(<%=arraycategory.get(10)%>)</b></div></td>
					</tr>
					<tr>
						<td class="info"><br></td>
					</tr>
				</table>
				<table  class="photo">
					<tr>
						<td class="info"><html:link href="search.do?sort=0&category_id=12" ><img src="img/etc.jpg" class="cateimg"></html:link></td>
					</tr>
					<tr>
						<td class="info"><div align="center"><b>その他(<%=arraycategory.get(11)%>)</b></div></td>
					</tr>
					<tr>
						<td class="info"><br></td>
					</tr>
				</table>
			</div>
			<!--  △メイン -->
		</div>
		<!--  ▲ワッパー -->
		<!--  ▼フッター -->
		<div id="footer">
			<a href="#">ページ上部に戻る</a>
		</div>
		<!--  ▲フッター -->
	</div>
</body>
</html:html>