function header(){
	document.write("<div id='header'>");
	document.write("	<a href='index.html'><img src='img/phopan.png' class='logo'></a>");
	document.write("	<div class='left-text'>");
	document.write("		写真投稿ブログ");
	document.write("	</div>");
	document.write("	<div class='right-text'>");
	document.write("		<a href='newaccount.html'>新規ユーザ登録</a> / <a href='login.html'>ログイン</a>");
	document.write("	</div>");
	document.write("</div>");
}

function headerlogin(){
	document.write("<div id='header'>");
	document.write("	<a href='index.html'><img src='img/phopan.png' class='logo'></a>");
	document.write("	<div class='left-text'>");
	document.write("		写真投稿ブログ");
	document.write("	</div>");
	document.write("	<div class='right-text'>");
	document.write("		<a href='mypage.html'>マイページ</a> / <a href='index.html'>ログアウト</a>");
	document.write("	</div>");
	document.write("</div>");
}


function search(){
	document.write("<div class='category'>");
	document.write("	<hr class='category-line'>");
	document.write("	<div class='menubase'>");
	document.write("		<ul id='cssmenu'>");
	document.write("			<li><a href='index.html'>トップ</a></li>");
	document.write("		</ul>");
	document.write("	</div>");
	document.write("	<form action='main.html' method='post'>");
	document.write("		<select name='category' class='search-box'>");
	document.write("			<option value='0'>全カテゴリ</option>");
	document.write("			<option value=''>風景・自然</option>");
	document.write("			<option value=''>人物</option>");
	document.write("			<option value=''>乗り物</option>");
	document.write("			<option value=''>動物</option>");
	document.write("			<option value=''>イラスト・アート</option>");
	document.write("			<option value=''>花・植物</option>");
	document.write("			<option value=''>建物・街並み</option>");
	document.write("			<option value=''>グルメ</option>");
	document.write("			<option value=''>イベント</option>");
	document.write("			<option value=''>インテリア・雑貨</option>");
	document.write("			<option value=''>スポーツ</option>");
	document.write("			<option value=''>その他</option>");
	document.write("		</select>");
	document.write("		<select name='sort' class='search-box'>");
	document.write("			<option value='0'>投稿日時が新しい順</option>");
	document.write("			<option value=''>投稿日時が古い順</option>");
	document.write("			<option value=''>閲覧数が多い順</option>");
	document.write("			<option value=''>閲覧数が少ない順</option>");
	document.write("			<option value=''>いいなぁが多い順</option>");
	document.write("			<option value=''>いいなぁが少ない順</option>");
	document.write("		</select>");
	document.write("		<input type='text' name='searchword' placeholder='検索ワードを入力してください'>");
	document.write("		<input type='submit' class='button' value='検索'>");
	document.write("	</form>");
	document.write("</div>");
}

function ranking(){
	document.write("<h3>いいなぁランキング</h3>");
	document.write("<div class='posi-right'>(0時更新)</div>");
	document.write("<h4>総合</h4>");
	document.write("<table class='rank'>");
	document.write("	<tr>");
	document.write("		<td class='ver-center' class='50px'><img src='img/material/oukan1.png' class='oukan'></td>");
	document.write("		<td class='posi-center'><a href='photo.html'><img src='img/dog4.jpg' class='ranking' ></a><div class='ranking-comment'>いいなぁ(120)</div></td>");
	document.write("	</tr>");
	document.write("	<tr>");
	document.write("		<td class='ver-center'><img src='img/material/pics2548.png' class='oukan'></td><td class='posi-center'><a href='photo.html'><img src='img/dog2.jpg' class='ranking' ></a><div class='ranking-comment'>いいなぁ(93)</div></td>");
	document.write("	</tr>");
	document.write("	<tr>");
	document.write("		<td class='ver-center'><img src='img/material/pics2547.png' class='oukan'></td><td class='posi-center'><a href='photo.html'><img src='img/dog3.jpg' class='ranking' ></a><div class='ranking-comment'>いいなぁ(70)</div></td>");
	document.write("	</tr>");
	document.write("</table><hr>");

	document.write("<h4 id='daily'>デイリー</h4>");
	document.write("<table class='rank'>");
	document.write("	<tr>");
	document.write("		<td><img src='img/material/oukan1.png' class='oukan'></td><td class='posi-center'><a href='photo.html'><img src='img/daibutu2.jpg' class='ranking' ></a><div class='ranking-comment'>いいなぁ(20)</div></td>");
	document.write("	</tr>");
	document.write("	<tr>");
	document.write("		<td><img src='img/material/pics2548.png' class='oukan'></td><td class='posi-center'><a href='photo.html'><img src='img/dog2.jpg' class='ranking' ></a><div class='ranking-comment'>いいなぁ(15)</div></td>");
	document.write("	</tr>");
	document.write("	<tr>");
	document.write("		<td><img src='img/material/pics2547.png' class='oukan'></td><td class='posi-center'><a href='photo.html'><img src='img/dog3.jpg' class='ranking' ></a><div class='ranking-comment'>いいなぁ(7)</div></td>");
	document.write("	</tr>");
	document.write("</table><hr>");

	document.write("<h4>ウィークリー</h4>");
	document.write("<table class='rank'>");
	document.write("	<tr>");
	document.write("		<td><img src='img/material/oukan1.png' class='oukan'></td><td class='posi-center'><a href='photo.html'><img src='img/dog1.jpg' class='ranking' ></a><div class='ranking-comment'>いいなぁ(50)</div></td>");
	document.write("	</tr>");
	document.write("	<tr>");
	document.write("		<td><img src='img/material/pics2548.png' class='oukan'></td><td class='posi-center'><a href='photo.html'><img src='img/niji.jpg' class='ranking' ></a><div class='ranking-comment'>いいなぁ(42)</div></td>");
	document.write("	</tr>");
	document.write("	<tr>");
	document.write("		<td><img src='img/material/pics2547.png' class='oukan'></td><td class='posi-center'><a href='photo.html'><img src='img/jet.jpg' class='ranking' ></a><div class='ranking-comment'>いいなぁ(29)</div></td>");
	document.write("	</tr>");
	document.write("</table>");

}

function allcheck(targetForm,flag){
	for(n=0;n<=targetForm.length-1;n++){
		if(targetForm.elements[n].type == "checkbox"){
			targetForm.elements[n].checked = flag;
		}
	}
}