<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="css/base.css" rel="stylesheet">
<title>phopan</title>
</head>
<body>
<%String imgid = request.getParameter("img_id"); %>
<h3>通報理由</h3>
		選択した画像で通報理由に該当する項目を1つ選んでください。
		<br>
		<br>
		<html:form action="reportPush">
			<table class="posi-center">
				<tr>
					<td class="posi-left"><input class="radio" type="radio" name="report" value="copyright" checked>著作権で問題が生じる投稿</td>
				</tr>
				<tr>
					<td class="posi-left"><input class="radio" type="radio" name="report" value="portrait">肖像権で問題が生じる投稿</td>
				</tr>
				<tr>
					<td class="posi-left"><input class="radio" type="radio" name="report" value="infringement">特定の個人に対する人権侵害、中傷、悪口で問題が生じる投稿</td>
				</tr>
				<tr>
					<td class="posi-left"><input class="radio" type="radio" name="report" value="sexual">差別的な表現、性的な表現、暴力的な表現などで問題が生じる投稿</td>
				</tr>
				<tr>

					<td class="posi-center"><input type="submit" value="確認"  class="button" ></td>
					<input type="hidden" name = "imgid" value =<%=imgid %>>
				</tr>

			</table>
		</html:form>
	</body>
</html>