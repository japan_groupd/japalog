<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<% String c = (String)request.getAttribute("c"); %>
<% String s = (String)request.getAttribute("s"); %>
<% String k = (String)request.getAttribute("k"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<body>
	<div class="category">
		<hr class="category-line">
		<div class="menubase">
			<ul id="cssmenu">
				<li><a href="index.jsp">トップ</a></li>
			</ul>
		</div>

		<html:form action="/search">
			<html:select property="category_id" styleClass="search-box" value="<%=c%>">
				<html:option value="0">全カテゴリ</html:option>
				<html:option value="1">風景・自然</html:option>
				<html:option value="2">人物</html:option>
				<html:option value="3">乗り物</html:option>
				<html:option value="4">動物</html:option>
				<html:option value="5">イラスト・アート</html:option>
				<html:option value="6">花・植物</html:option>
				<html:option value="7">建物・街並み</html:option>
				<html:option value="8">グルメ</html:option>
				<html:option value="9">イベント</html:option>
				<html:option value="10">インテリア・雑貨</html:option>
				<html:option value="11">スポーツ</html:option>
				<html:option value="12">その他</html:option>
			</html:select>
			<html:select property="sort" styleClass="search-box" value="<%=s%>">
				<html:option value="0">投稿日時が新しい順</html:option>
				<html:option value="1">投稿日時が古い順</html:option>
				<html:option value="2">いいなぁが多い順</html:option>
				<html:option value="3">いいなぁが少ない順</html:option>
			</html:select>
			<html:text property="keyword" value="<%=k%>" />
			<html:submit styleClass="button" value="検索" />
		</html:form>
	</div>
</body>
</html:html>
