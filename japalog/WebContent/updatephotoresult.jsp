<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<title>phopan</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="js/calendar.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<link type="text/css" href="css/base.css" rel="stylesheet">
	</head>
	<body>
		<div id="basesize">
				<!--  ▼ヘッダー -->
			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
			<jsp:include page="/search.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
			<!--  ▼ワッパー -->
			<div id="wrapper">
				<div id="main" class="column">
					<table class="posi-center">
						<tr>
							<td class="posi-center"><h2>投稿内容を更新しました</h2></td>
						</tr>
						<tr>
							<td class="posi-center">
								<form action="mypage.jsp">
									<input type="submit"  class="button" value="マイページへ">
								</form>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<!--  ▲ワッパー -->
			<!--  ▼フッター -->
			<div id="footer">
				<a href="#basesize">ページ上部に戻る</a>
			</div>
			<!--  ▲フッター -->
		</div>
	</body>
</html>