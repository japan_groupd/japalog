<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<% String error = (String)request.getAttribute("error"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link type="text/css" href="css/base.css" rel="stylesheet">
	<script type="text/javascript" src="js/main.js"></script>
<title>phopan</title>
</head>
<body>
	<div id="basesize">
		<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/header.jsp" flush="true" />
			<jsp:include page="/search.jsp" flush="true" />
		<!--  ▲ヘッダー＆カテゴリ -->
		<!--  ▼ワッパー -->
		<div id="wrapper">
			<!--  △メニュー -->
			<div id="main" class="posi-center"><font color="red"><%if(error != null){%><%=error %><br><%}%><html:errors/></font>
				<html:form action="/login" onsubmit="return validateLoginForm(this)" >
					<table class="edit posi-center">
						<tr>
							<td><h2>ログイン</h2></td>
						</tr>
						<tr>
							<td><h6>ユーザ名とパスワードを入力して下さい</h6></td>
						</tr>
						<tr>
							<td class="posi-left"><font color="red">※</font>ユーザ名</td>
						</tr>
						<tr>
							<td><input type="text" name="user_name" value=""><br></td>
						</tr>
						<tr>
							<td class="posi-left"><font color="red">※</font>パスワード</td>
						</tr>
						<tr>
							<td><input type="password" name="pass" value=""><br></td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" class="button"
								value="ログイン"></td>
						</tr>

					</table>
				</html:form>
			</div>
		</div>
		<!--  ▲ワッパー -->
		<!--  ▼フッター -->
		<div id="footer">
			<a href="#">ページ上部に戻る</a>
		</div>
		<!--  ▲フッター -->
	</div>
</body>
</html:html>