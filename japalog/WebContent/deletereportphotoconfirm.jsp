<%@page import="jp.co.phopan.db.ImageTable"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%ArrayList<ImageTable> img = (ArrayList<ImageTable>) request.getAttribute("deletephoto");%>

<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="css/adminbase.css" rel="stylesheet">
<script type="text/javascript" src="js/main.js"></script>
<title>phopan</title>
</head>
<body>
	<div id="basesize">

			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/headeradmin.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
		<!--  ▼ワッパー -->
		<div id="wrapper">
			<div id="main" class="column">
				<h3>管理画面</h3>
				<br>
				<table class="posi-center">
					<tr>
						<td><h1>通報写真削除</h1></td>
					<tr>
						<td>本当にこの写真を削除しますか?</td>
					</tr>
				</table>
				<table class="posi-center">
				<html:form action="/deleteReportPhotoConfirm">
					<tr>
						<td>
							<table border="1" class="admin posi-center">
								<tr>
									<th class="id">ID</th>
									<th class="title">タイトル</th>
									<th class="report">通報数</th>
									<th class="report">著作権</th>
									<th class="report">肖像権</th>
									<th class="report">人権侵害</th>
									<th class="report">不適切</th>
									<th class="photo">写真</th>
								</tr>
								<%for(int i=0;i<img.size();i++) {%>
								<tr>
									<td><%=img.get(i).getImg_id() %></td>
									<td><%=img.get(i).getTitle() %></td>
									<td><%=img.get(i).getReport() %></td>
									<td><%=img.get(i).getCopyright() %></td>
									<td><%=img.get(i).getPortrait() %></td>
									<td><%=img.get(i).getInfringement() %></td>
									<td><%=img.get(i).getSexual() %></td>
									<td><img src ='contents/<%= img.get(i).getImg_name() %>' class="admiimg">
									<html:hidden property="img_id"/>
									</td>
								</tr>
								<%} %>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan=4>
							<html:submit property="delete" value="削除" styleClass="button"/>
							<input type="button" class="button" onClick="document.location='deletereportphoto.jsp';" value="戻る">
						</td>
					</tr>
					</html:form>
				</table>
			</div>
		</div>
		<!--  ▲ワッパー -->
		<!--  ▼フッター -->
		<div id="footer">
			<a href="#basesize">ページ上部に戻る</a>
		</div>
		<!--  ▲フッター -->
	</div>
</body>

</html:html>