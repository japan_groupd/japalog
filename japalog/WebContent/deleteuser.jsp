<%@page import="jp.co.phopan.db.DbUtil"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="jp.co.phopan.db.UserTable"%>
<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
	String deleteresult = (String) request.getAttribute("result");
	String err = (String) request.getAttribute("error");
%>

<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="css/adminbase.css" rel="stylesheet">
<script type="text/javascript" src="js/main.js"></script>
<title>phopan</title>
</head>
<body>
	<div id="basesize">

			<!--  ▼ヘッダー＆カテゴリ -->
			<jsp:include page="/headeradmin.jsp" flush="true" />
			<!--  ▲ヘッダー＆カテゴリ -->
		<!--  ▼ワッパー -->
		<div id="wrapper">
			<div id="main">

				<!-- ▼ UserTable検索 -->
				<%
					Connection con=DbUtil.connect();
					String sql1 = "SELECT*FROM UserTable WHERE user_delete=true;";
					PreparedStatement stmt = con.prepareStatement(sql1);
					ResultSet rs = stmt.executeQuery();
					ArrayList<UserTable> userresult = new ArrayList<UserTable>();


					while (rs.next()) {
						UserTable user = new UserTable();
							user.setUser_id(rs.getInt("user_id"));
							user.setUser_name(rs.getString("user_name"));
							user.setPass(rs.getString("pass"));
							userresult.add(user);
					}
					DbUtil.disconnect(con);
				%>

				<!--  ▲UserTable検索  -->

				<h3>管理画面</h3>
				<br>
				<table class="admin-mini posi-center">
					<tr>
						<td><h1>ユーザ削除</h1></td>
					</tr>
					<tr>
						<td><%if (deleteresult != null) {%><font color="red"><%=deleteresult%></font><%}%></td>
					</tr>
					<tr>
						<td>消したいユーザを削除</td>
					</tr>
				</table>
				<%if (err != null) {%>
					<font color="red"><%=err%></font>
				<%}%>
				<table class="posi-ccenter">

						<tr>
							<td>
								<table border="1" class="admin-mini posi-center">
									<tr>
										<th class="id">ID</th>
										<th>ユーザ名</th>
										<th class="pw">パスワード</th>
									</tr>
										<%for(int i=0;i< userresult.size();i++) {%>
											<tr>
												<td><%=userresult.get(i).getUser_id() %></td>
												<td><%=userresult.get(i).getUser_name() %></td>
												<td><%=userresult.get(i).getPass() %></td>
												<html:form action="/deleteUser" method="POST">
												<input type="hidden" name="user_id" value="<%=userresult.get(i).getUser_id()%>">
												<input type="hidden" name="user_name" value="<%=userresult.get(i).getUser_name()%>">
												<input type="hidden" name="pass" value="<%=userresult.get(i).getPass()%>">

												<td class="delete"><html:submit property="delete" value="削除" styleClass="button"/></td>
												</html:form>
											</tr>
										<%} %>
								</table>
							</td>
						</tr>
				</table>
			</div>
		</div>
		<!--  ▲ワッパー -->
		<!--  ▼フッター -->
		<div id="footer">
			<a href="#basesize">ページ上部に戻る</a>
		</div>
		<!--  ▲フッター -->
	</div>
</body>
</html:html>